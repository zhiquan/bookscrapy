var models = require('../model');
Book = models.Book;



exports.getBooks = function(level){
	return Book.find().sort({title:1}).exec()
}


exports.getBooksByBatch = function(batch){
	return Book.find().sort({title:1}).skip(batch.start).limit(batch.pageCount).exec()
}

exports.getBookById = function(id){
	return Book.findOne({_id:id}).exec()
}


exports.searchBooksByCondition = function(condition){

	var searchRegex = '.*' + condition.searchWord + '.*'

	return Book.find({'$or' : [{ title : {'$regex':searchRegex} }, { author : {'$regex':searchRegex} }] })
				.sort({title:1})
				.skip(condition.start)
				.limit(condition.pageCount)
				.exec()
}


exports.getBooksByCategoryCondition = function(condition){

	console.log('condition.category', condition.category)

	var regex = new RegExp(condition.category, 'i');
	console.log(regex)

	return Book.find({ 'category.content' : {'$regex': regex }})
				.sort({title:1})
				.skip(condition.start)
				.limit(condition.pageCount)
				.exec()
}


exports.getBooksByAuthorCondition = function(condition){

	return Book.find({ author : condition.author })
				.sort({title:1})
				.skip(condition.start)
				.limit(condition.pageCount)
				.exec()
}