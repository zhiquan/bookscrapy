var models = require('../model');
Category = models.Category;


exports.getCategoriesByLevel = function(level){
	return Category.find({level:level}).sort({name:1}).exec()
}

exports.getCategoriesByParent = function(name){
	return Category.find({parent:name}).sort({name:1}).exec()
}