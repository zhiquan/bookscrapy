'use strict'

var config = require('./config');
var koa = require('koa');
var router = require('./router');
var logger = require('koa-logger');
var render = require('koa-ejs');
var path = require('path');
var koaStatic = require('koa-static');
var bodyParser = require('koa-bodyparser');


var app = koa()

app.use(logger());
app.use(koaStatic('public'));
app.use(bodyParser());
app.use(router.routes());
app.use(router.allowedMethods());




render(app, {
  root : path.join(__dirname, 'view'),
  layout : false,
  viewExt: 'html',
  cache: false,
  debug: true
});


app.listen(config.port);

console.log('Server started at port '+ config.port);