
var bookApp = angular.module('bookApp', ['ngRoute']);

//路由
function bookRouteConfig($routeProvider) {
  $routeProvider.
  when('/', {
    controller: bookListCtrl,
    templateUrl: 'bookList.html'
  }).
  when('/view/:id', { //在id前面加一个冒号，从而制订了一个参数化URL
    controller: bookDetailCtrl,
    templateUrl: 'bookDetail.html'
  }).
  when('/search', {
    controller: bookSearchCtrl,
    templateUrl: 'bookSearch.html'
  }).
  when('/category', {
    controller : categoryCtrl,
    templateUrl : 'category.html'
  }).
  when('/category/:categoryName', {
    controller : categorySearchCtrl,
    templateUrl : 'categorySearch.html'
  }).
  when('/author', {
    controller : authorCtrl,
    templateUrl : 'author.html'
  }).
  when('/author/:authorName', {
    controller : authorSearchCtrl,
    templateUrl : 'authorSearch.html'
  }).
  otherwise({
    redirectTo: '/'
  });
}

bookApp.config(bookRouteConfig);//配置我们的路由
bookApp.controller('bookCtrl', bookCtrl);
bookApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});



function bookListCtrl($scope, $http){
  $scope.pageCount = 10
  $scope.navPageNumber = []

  $scope.loadBooks = function(start){
    //console.log('start', start)
    $http.get('/books?start='+ start).
      success(function(books, status, headers, config){
        //console.log(books)
        if (Array.isArray(books)) {
          $scope.books = books
          
          $scope.currentPage = start/$scope.pageCount
          $scope.navPageNumber = []
          var pageIndex = $scope.currentPage

          $scope.previousPage = pageIndex - 1
          if($scope.previousPage < 0) $scope.previousPage = 0

          $scope.nextPage = pageIndex + 1

          count = 0
          while(pageIndex >=0){
            if(count >=5) break
            $scope.navPageNumber.unshift(pageIndex)
            pageIndex--
            count++
          }

          for(var index = $scope.currentPage + 1; index <= $scope.currentPage + (10-count); index++ ){
            $scope.navPageNumber.push(index)
          }

          //console.log($scope.navPageNumber)

        }else{
          alert('加载类型失败');
        }
      }).
      error(function(data, status, headers, config){
        alert('加载类型失败');
      });
  };

  $scope.loadBooks(0);
}

function bookDetailCtrl($scope, $http, $routeParams) {
  $http.get('/books/'+$routeParams.id).
     success(function(book, status, headers, config){
        $scope.book = book
        //console.log(book)
     }).
     error(function(data, status, headers, config){
        alert('获取书籍失败!');
     })
}

function bookSearchCtrl($scope, $http, $location){

  $scope.navPageNumber = []
  $scope.pageCount = 10
  $scope.search = function(start, searchWord){

    $http.get('/search?start='+ start + '&searchWord='+searchWord).
      success(function(books, status, headers, config){
        //console.log(books)
        if (Array.isArray(books)) {
          $scope.books = books
          
          $scope.currentPage = start/$scope.pageCount
          $scope.navPageNumber = []
          var pageIndex = $scope.currentPage

          $scope.previousPage = pageIndex - 1
          if($scope.previousPage < 0) $scope.previousPage = 0

          $scope.nextPage = pageIndex + 1

          count = 0
          while(pageIndex >=0){
            if(count >=5) break
            $scope.navPageNumber.unshift(pageIndex)
            pageIndex--
            count++
          }

          for(var index = $scope.currentPage + 1; index <= $scope.currentPage + (10-count); index++ ){
            $scope.navPageNumber.push(index)
          }

          //console.log($scope.books)
          //console.log($scope.navPageNumber)



        }else{
          alert('查询书籍失败！');
        }
      }).
      error(function(data, status, headers, config){
        alert('查询书籍失败！');
      });
  };

  $scope.navPage = function(start, searchWord){
    $location.url('/search?start='+ start +'&searchWord='+searchWord)
  }

  $scope.searchWord = $location.search().searchWord
  if($scope.searchWord == undefined ) $scope.searchWord = ''

  $scope.search($location.search().start, $scope.searchWord)
}

function categoryCtrl($scope, $http){
  loadCategories();
  //console.log('start')
  function loadCategories(){
    $http.get('/categories').
      success(function(categories, status, headers, config){
        //console.log(categories)
        if (Array.isArray(categories)) {
          $scope.categories = categories;
        }else{
          alert('加载类型失败');
        }
      }).
      error(function(data, status, headers, config){
        alert('加载类型失败');
      });
  };
}

function categorySearchCtrl($scope, $http, $routeParams){
  $scope.navPageNumber = []
  $scope.pageCount = 10

  $scope.categorySearch = function(start, categoryName){
      $http.get('/book/category?start='+start + '&category=' + categoryName).
      success(function(books, status, headers, config){
        //console.log(books)
        if (Array.isArray(books)) {
          $scope.books = books
          
          $scope.currentPage = start/$scope.pageCount
          $scope.navPageNumber = []
          var pageIndex = $scope.currentPage

          $scope.previousPage = pageIndex - 1
          if($scope.previousPage < 0) $scope.previousPage = 0

          $scope.nextPage = pageIndex + 1

          count = 0
          while(pageIndex >=0){
            if(count >=5) break
            $scope.navPageNumber.unshift(pageIndex)
            pageIndex--
            count++
          }

          for(var index = $scope.currentPage + 1; index <= $scope.currentPage + (10-count); index++ ){
            $scope.navPageNumber.push(index)
          }

          //console.log($scope.books)
          //console.log($scope.navPageNumber)

        }else{
          alert('查询书籍失败！');
        }
      }).
      error(function(data, status, headers, config){
        alert('查询书籍失败！');
      });
  }

  $scope.navPage = function(start, categoryName){
    $scope.categorySearch(start, categoryName)
  }

  $scope.category = $routeParams.categoryName
  //console.log('$routeParams.categoryName', $routeParams.categoryName)
  $scope.categorySearch(0, $routeParams.categoryName)
}

function authorCtrl($scope, $http){
  loadAuthors();
  //console.log('start')
  function loadAuthors(){
    $http.get('/authors').
      success(function(authors, status, headers, config){
        console.log('authors:',authors)
        if (Array.isArray(authors)) {
          $scope.authors = authors;
        }else{
          alert('加载作者失败');
        }
      }).
      error(function(data, status, headers, config){
        alert('加载作者失败');
      });
  };
}

function authorSearchCtrl($scope, $http, $routeParams){
  $scope.navPageNumber = []
  $scope.pageCount = 10

  $scope.authorSearch = function(start, authorName){
      $http.get('/book/author?start='+start + '&author=' + authorName).
      success(function(books, status, headers, config){
        //console.log(books)
        if (Array.isArray(books)) {
          $scope.books = books
          
          $scope.currentPage = start/$scope.pageCount
          $scope.navPageNumber = []
          var pageIndex = $scope.currentPage

          $scope.previousPage = pageIndex - 1
          if($scope.previousPage < 0) $scope.previousPage = 0

          $scope.nextPage = pageIndex + 1

          count = 0
          while(pageIndex >=0){
            if(count >=5) break
            $scope.navPageNumber.unshift(pageIndex)
            pageIndex--
            count++
          }

          for(var index = $scope.currentPage + 1; index <= $scope.currentPage + (10-count); index++ ){
            $scope.navPageNumber.push(index)
          }

          //console.log($scope.books)
          //console.log($scope.navPageNumber)

        }else{
          alert('查询书籍失败！');
        }
      }).
      error(function(data, status, headers, config){
        alert('查询书籍失败！');
      });
  }

  $scope.navPage = function(start, authorName){
    $scope.authorSearch(start, authorName)
  }

  $scope.author = $routeParams.authorName
  //console.log('$routeParams.categoryName', $routeParams.categoryName)
  $scope.authorSearch(0, $routeParams.authorName)  
}

function bookCtrl($scope, $location){
  $scope.search = function(){
    $location.url('/search?start=0&searchWord='+$scope.searchWord)
  }
}






