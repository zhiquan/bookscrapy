'use strict'


var Category = require('../proxy').Category;



exports.categoryPage = function *(next){
	yield this.render('category');
};


exports.listCategory = function *(next){
	
	var topCategories = yield Category.getCategoriesByLevel("1");

	var childCategories = yield topCategories.map(function(topCategory){
		return Category.getCategoriesByParent(topCategory.name)
	})

	var categories = topCategories.map(function(topCategory, index){
		var category_obj = topCategory.toObject()
		category_obj['childs'] = childCategories[index]
		return category_obj
	})


	this.body = categories
}