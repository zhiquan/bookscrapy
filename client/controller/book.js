'use strict'

var config = require('../config')
var Book = require('../proxy').Book



exports.listBook = function *(next){

	console.log(this.request.query)

	var batch = {}
	batch.start = this.request.query.start
	batch.pageCount = config.bookPageCount

	var books = yield Book.getBooksByBatch(batch);

	//console.log('books', books)

	this.body = books
}

exports.findBookById = function *(next){
	console.log('this.params.id',this.params.id)

	var book = yield Book.getBookById(this.params.id)

	//console.log('book', book)

	this.body = book
}


exports.search = function *(next){
	console.log(this.request.query)

	var condition = {}
	condition.start = parseInt(this.request.query.start)
	condition.searchWord = this.request.query.searchWord
	condition.pageCount = config.bookPageCount

	//console.log(condition)

	var books = yield Book.searchBooksByCondition(condition)

	console.log('books', books)

	this.body = books
}

exports.findBooksByCategory = function *(next){
	console.log('this.request.query',this.request.query)

	var categoryCondition = {}
	categoryCondition.start = parseInt(this.request.query.start)
	categoryCondition.category = this.request.query.category
	categoryCondition.pageCount = config.bookPageCount

	var books = yield Book.getBooksByCategoryCondition(categoryCondition)

	//console.log('books', books)

	this.body = books
}


exports.findBooksByAuthor = function *(next){
	console.log('this.request.query',this.request.query)

	var authorCondition = {}
	authorCondition.start = parseInt(this.request.query.start)
	authorCondition.author = this.request.query.author
	authorCondition.pageCount = config.bookPageCount

	var books = yield Book.getBooksByAuthorCondition(authorCondition)

	//console.log('books', books)

	this.body = books
}
