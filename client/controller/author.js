'use strict'


var Author = require('../proxy').Author;


exports.listAuthor = function *(next){
	var authors = yield Author.getAuthors()

	//console.log('authors', authors)
	this.body = authors
}