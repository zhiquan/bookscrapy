'use strict'


var router = require('koa-router')();
module.exports = router;
var category = require('./controller/category');
var author = require('./controller/author')
var book = require('./controller/book')


module.exports = router;



router.get('/', function *(next){ yield this.render('index'); });

//API-JSON
router.get('/books', book.listBook)
router.get('/books/:id', book.findBookById)
router.get('/search', book.search)
router.get('/book/category', book.findBooksByCategory)
router.get('/book/author', book.findBooksByAuthor)

router.get('/categories', category.listCategory)
router.get('/authors', author.listAuthor)

router.get(/(\w+).html/i, function *(next){ yield this.render(this.params['0']) })