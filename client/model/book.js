var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BookSchema = new Schema({
  title : { type: String },
  author : { type: String },

  synopsis : { content: String, fromurl: String, spider: String, fitness:Number},
  author_intro : { content: String, fromurl: String, spider: String, fitness:Number},
  category : { content: String , count: Number},

  face : { fromurl: String, spider: String, localpath: String, downloadurl: String, fitness:Number},

  fitness : { type: Number},
  covers : [{ fromurl: String, spider: String, localpath: String, downloadurl: String, fitness:Number}],
  books : [{ fromurl: String, spider: String, localpath: String, downloadurl: String, fitness:Number}]
},{ collection: 'interface_book' });


mongoose.model('book', BookSchema);