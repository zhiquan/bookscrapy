var mongoose = require('mongoose');
var config = require('../config');

//connect mongodb
mongoose.connect(config.db, function (err) {
  if (err) {
    console.error('connect to %s error: ', config.db, err.message);
    process.exit(1);
  }
});

// models
require('./author')
require('./category');
require('./book')

exports.Category = mongoose.model('category');
exports.Book = mongoose.model('book');
exports.Author = mongoose.model('author');