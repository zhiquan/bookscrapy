var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategorySchema = new Schema({
  name : { type: String },
  parent : { type: String },
  level : { type: String },
  count : { type: String },
  amount : { type: Number }, 
});


mongoose.model('category', CategorySchema);