# -*- coding: utf-8 -*-


from pymongo import MongoClient
import config


mongoclient = MongoClient(config.db_uri)
db = mongoclient[config.db_name]


