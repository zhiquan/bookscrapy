#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path


from tornado.web import Application
from tornado.ioloop import IOLoop
import tornado.platform.twisted
tornado.platform.twisted.install()
IOLoop.instance().set_blocking_log_threshold(0.5)


import config 
from handler import WSHandler,SpiderHandler, SpiderHisHandler,\
	 BookHandler,SrcBookHandler,SiteBookHandler, CleanHandler,\
	 CategoryHandler,AuthorHandler,StatisticsHandler

handlers = [
	(r"/spider", SpiderHandler),
	(r"/spider/(.*)/(.*)", SpiderHisHandler),
	(r"/spider/(.*)/", SpiderHisHandler),

	(r"/book", BookHandler),
	(r"/book/(\w+)", BookHandler),
	#(r"/book/(\w+)/(.*)", BookHandler)
	(r"/book/(\w+)/(\w+)/(\w+)", BookHandler),
	(r"/book/category/(.*)", BookHandler),
	(r"/book/author/(.*)", BookHandler),
	

	(r"/srcBook", SrcBookHandler),
	(r"/srcBook/(\w+)/(\w+)/(\w+)", SrcBookHandler),
	(r"/siteBook/(.*)/(.*)", SiteBookHandler),

	(r"/category", CategoryHandler),
	(r"/categories", CategoryHandler),
	(r"/categories/(\w+)", CategoryHandler),

	(r"/author", AuthorHandler),
	(r"/author/new", AuthorHandler),
	(r"/author/delete", AuthorHandler),

	(r'/clean', CleanHandler),
	(r'/clean/(\w+)/(\w+)',CleanHandler),

	(r'/statistics', StatisticsHandler),
	
	(r"/send", WSHandler)
]


def _load_spiders():
	from scrapy.utils.project import get_project_settings
	from scrapy.spiderloader import SpiderLoader
	from proxy.spiderProxy import spiderProxy

	scrapy_setting = get_project_settings()
	spider_loader = SpiderLoader(scrapy_setting)
	spider_names = spider_loader.list()

	for name in spider_names:
		spider = spider_loader.load(name)
		spider_info = spiderProxy.get(name)
		#print 'spider_info:', spider_info, name
		print 'spider.status:', spider.status
		if spider.status == 'unready':
			continue

		if spider_info is None:
			#print '1spider_info:', spider_info, name
			spiderProxy.save_from_cls(spider)
			#continue
		else:
			reset_spider = spiderProxy.reset(spider_info)
			spiderProxy.update(name, reset_spider)

def _config_log():
	import logging
	from scrapy.utils.project import get_project_settings
	from util import WSLogHandler
	handler = WSLogHandler()
	settings = get_project_settings()
	formatter = logging.Formatter(
        fmt=settings.get('LOG_FORMAT'),
        datefmt=settings.get('LOG_DATEFORMAT')
    )
	handler.setFormatter(formatter)
	handler.setLevel(settings.get('LOG_LEVEL'))

	logging.root.addHandler(handler)

def _start_reactor():
	template_path = os.path.join(os.path.dirname(__file__), "template")
	static_path = os.path.join(os.path.dirname(__file__), "static")

	app = Application(handlers=handlers, 
					  template_path=template_path, 
					  static_path=static_path,
					  debug = True)
	app.listen(config.port,config.host)
	print "bookscrapy is started on %s:%s" % (config.host, config.port)
	
	# we use CrawlerProcess to start the reactor.
	from util.crawler_process import crawler_process
	crawler_process.start(stop_after_crawl=False)

def _config_timer():
	import task
	clean_task = task.TimerTask(config.clean_interval, None, task.do_clean)
	clean_task.start()

	check_task = task.TimerTask(config.check_interval, None, task.do_check)
	check_task.start()

	statistics_task = task.TimerTask(config.statistic_interval, None, task.do_statistic)
	statistics_task.start()


def app():
	#tornado.options.parse_command_line()
	_load_spiders()
	_config_log()
	_config_timer()
	_start_reactor()


if __name__ == "__main__":
	print 'hello world start'

	app()
	#import task
	#task.do_check()


	print 'hello word end'
	

