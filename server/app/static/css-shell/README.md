# CSS Shell

A basic Mac Terminal look-alike using HTML and CSS. It should look good in all modern browsers, including IE8+

Screenshot:
![alt text](https://raw.githubusercontent.com/pjlangley/css-shell/master/screenshot.png "CSS Shell Screenshot")

See the demo here; [http://www.codechewing.com/demo/css-shell/](http://www.codechewing.com/demo/css-shell/)