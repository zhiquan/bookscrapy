var categoryModule = angular.module('CategoryModule', []);

categoryModule.controller('CategoryCtrl', categoryCtrl);


function categoryCtrl($scope, $http){

	$scope.newCategory = function(){
		//console.log($scope.name, $scope.code, $scope.startNO, $scope.endNO, $scope.amount);
		if (!$scope.name  || !$scope.level  ) {
			alert('请填入合法的值!');
			return;
		};

		if (!$scope.parent){
			$scope.parent = ''
		}

		var category = { name:$scope.name, 
						 level:$scope.level, 
						 parent:$scope.parent,
						 count:0 
						};
		//console.log('category:', category)

		$http.post('/categories', category).
			success(function(savedCategory, status, headers, config) {
				//console.log('savedCategory:', savedCategory)
    			if (savedCategory._id) {

    				alert('添加成功');
    				$('#categoryAddView').modal('toggle');
    				location.reload()
    			} else {

    				alert('添加失败'+savedCategory);

    			}
  			}).
  			error(function(data, status, headers, config) {
    			alert('添加失败'+data);
  			});
	}

	$scope.deleteCategory = function(index){
		var id = $('.' + index).text()

		//console.log(id)
		$http.delete('/categories/'+id).
			success(function(result, status, headers, config){
				location.reload()
			}).
			error(function(data, status, headers, config){
				alert(data+status)
			})
	}

	
}