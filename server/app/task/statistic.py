#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pymongo
import singleton
import config


class Statistic(object):
	stat_author_col = singleton.db[config.db_col_name_stat_author]
	stat_category_col = singleton.db[config.db_col_name_stat_category]
	interface_col = singleton.db[config.db_col_name_interface_book]

	def __init__(self):
		pass

	def do_statistic(self):
		self.stat_author_col.drop()
		self.stat_category_col.drop()

		self.gather_category_info()
		self.gather_author_info()


	def gather_category_info(self):
		stat_category_dict = {}
		done = False
		skip = 0
		while not done:
		    cursor = self.interface_col.find()
		    cursor.sort('insert_time') # recommended to use time or other sequential parameter.
		    cursor.skip(skip)
		    try:
		        for doc in cursor:
		            skip += 1
		            current_category_dict = self._stat_category_on_interface_document(doc)
		            for category in current_category_dict:
		            	current_count = current_category_dict[category]
		            	stat_category_dict[category] = stat_category_dict.setdefault(category, 0) + current_count
		        done = True
		    except pymongo.errors.OperationFailure, e:
		        msg = e.message
		        if not (msg.startswith("cursor id") and msg.endswith("not valid at server")):
		            raise

		#save category into db mongo
		from proxy import statCategoryProxy
		statCategoryProxy.save_from_dict(stat_category_dict)


	def gather_author_info(self):
		stat_author_dict = {}
		done = False
		skip = 0
		while not done:
		    cursor = self.interface_col.find()
		    cursor.sort('insert_time') # recommended to use time or other sequential parameter.
		    cursor.skip(skip)
		    try:
		        for doc in cursor:
		            skip += 1
		            current_author_dict = self._stat_author_on_interface_document(doc)
		            for author in current_author_dict:
		            	current_count = current_author_dict[author]
		            	stat_author_dict[author] = stat_author_dict.setdefault(author, 0) + current_count
		        done = True
		    except pymongo.errors.OperationFailure, e:
		        msg = e.message
		        if not (msg.startswith("cursor id") and msg.endswith("not valid at server")):
		            raise

		#save category into db mongo
		from proxy import statAuthorProxy
		statAuthorProxy.save_from_dict(stat_author_dict)

	def _stat_category_on_interface_document(self, interface_book):
		stat_category_dict = {}
		if interface_book['category']['content'] == '' or interface_book['category']['content'] == '||':
			stat_category_dict[''] = 1
			return stat_category_dict
			
		categories = interface_book['category']['content'].split('|')

		for category in categories:
			if category == '':
				continue
			stat_category_dict[category] = stat_category_dict.setdefault(category, 0) + 1

		return stat_category_dict


	def _stat_author_on_interface_document(self, interface_book):
		return {interface_book['author'] : 1}




def do_statistic():
	print '\nstatistic start up'
	
	stat = Statistic()
	stat.do_statistic()

	print 'statistic end up'


if __name__ == '__main__':
	do_statistic()