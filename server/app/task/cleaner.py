#!/usr/bin/env python
# -*- coding: utf-8 -*-


#import time
import pymongo
from datetime import datetime
import singleton
import config
from crawler.items import InterfaceBookItem
from proxy import spiderProxy


class Cleaner(object):

	interface_col = singleton.db[config.db_col_name_interface_book]
	interface_col.create_index('insert_time')
	
	central_col = singleton.db[config.db_col_name_central_book]
                    

	def __init__(self):
		from proxy import cleanProxy
		self.clean_info = cleanProxy.new_clean()

	def do_clean(self):
		self.clean_info['start_time'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
		self.merge_central_to_interface()
		self.gather_crawler_document_info()
		self.save_clean_info()

	def save_clean_info(self):
		self.clean_info['end_time'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
		from proxy import cleanProxy
		cleanProxy.save_clean(self.clean_info)

	def merge_central_to_interface(self):
		done = False
		skip = 0
		while not done:
		    cursor = self.central_col.find()
		    cursor.sort('insert_time') # recommended to use time or other sequential parameter.
		    cursor.skip(skip)
		    try:
		        for doc in cursor:
		            skip += 1
		            self._operate_central_book(doc)
		        done = True
		    except pymongo.errors.OperationFailure, e:
		        msg = e.message
		        if not (msg.startswith("cursor id") and msg.endswith("not valid at server")):
		            raise
		
	def gather_crawler_document_info(self):
		for spider in spiderProxy.list():
			spider_site_collection = singleton.db[spider['name']]
			self.clean_info['crawler_document_info'][spider['name']] = spider_site_collection.count()

	def _operate_central_book(self, central_book):

			if cmp(central_book['title'], '') == 0:
				self.clean_info['skip_count'] = self.clean_info['skip_count']+1
				return

			if len(central_book['books']) == 0:
				self.clean_info['skip_count'] = self.clean_info['skip_count']+1
				return

			tmp = self.interface_col.find_one({'author':central_book['author'], 'title':central_book['title']})
			if tmp is None:
				interface_book = self._new_interface_book(central_book)
				self.interface_col.insert_one(interface_book)
				self.clean_info['insert_count'] = self.clean_info['insert_count'] + 1
				self.clean_info['insert_set'].append(interface_book['_id'])
			else:
				joined_book = self._join_interface_book(tmp, central_book)
				if joined_book:
					self.interface_col.replace_one({'author':central_book['author'], 'title':central_book['title']}, joined_book)
					self.clean_info['update_count'] = self.clean_info['update_count'] + 1
					self.clean_info['update_set'].append(joined_book['_id'])


	def _new_interface_book(self,  central_book):

		interface_book = InterfaceBookItem()
		interface_book['author'] = central_book['author']
		interface_book['title'] = central_book['title']

		interface_book['synopsis'] = self._get_fitness_value(central_book['synopsises'])
		interface_book['author_intro'] = self._get_fitness_value(central_book['author_introes'])

		interface_book['face'] = self._get_face_value(central_book['covers'])
		interface_book['category'] = self._get_category_value(central_book['categories'])

		interface_book['covers'] = self._get_download_values(central_book['covers'])
		interface_book['books'] = self._get_download_values(central_book['books'])

		interface_book['fitness'] = 0
		interface_book['insert_time'] = datetime.now()
		
		return interface_book
	
	def _join_interface_book(self, interface_book, central_book):
		
		changed = False
		synopsis = self._get_fitness_value(central_book['synopsises'])
		
		#if central_book['title'] == u'活着活着就老了' and central_book['author'] == u'冯唐':
		#	print synopsis, interface_book['synopsis']
		if synopsis and (interface_book['synopsis']['fitness'] < synopsis['fitness'] \
						 or (interface_book['synopsis']['fitness']==0 
						 	and interface_book['synopsis']['content'] != synopsis['content']) ):
			interface_book['synopsis'] = synopsis
			changed = True


		author_intro = self._get_fitness_value(central_book['author_introes'])
		if author_intro and (interface_book['author_intro']['fitness'] < author_intro['fitness'] \
								or (interface_book['author_intro']['fitness'] == 0 
									and interface_book['author_intro']['content'] != author_intro['content'])):
			interface_book['author_intro'] = author_intro
			changed = True


		face = self._get_face_value(central_book['covers'])
		if face and (interface_book['face']['fitness'] < face['fitness'] \
			or (interface_book['face']['fitness'] == 0 
				and interface_book['face']['downloadurl'] != face['downloadurl'])):
			interface_book['face'] = face
			changed = True

		#add after database have data
		category = self._get_category_value(central_book['categories'])
		if 'category' not in interface_book.keys():
			interface_book['category'] = self._get_category_value(central_book['categories'])
			changed = True
		else:
			if category and (interface_book['category']['fitness'] < category['fitness'] \
				or (interface_book['category']['fitness'] == 0 
						and interface_book['category']['content'] != category['content']) ):
				interface_book['category'] = category
				changed = True

		download_keys = ['covers', 'books']
		for key in download_keys:
			for central_item in central_book[key]:
				exist = False
				for interface_item in interface_book[key]:
					#print '\n',interface_book[key], type(interface_item),key, 'interface_item', interface_item, 'central_item:', central_item
					if cmp(interface_item[u'downloadurl'], central_item[u'downloadurl']) == 0:
						exist = True
						break
				if not exist:
					interface_book[key].append(central_item)
					changed = True

			interface_book[key] = self._get_download_values(interface_book[key])

		if changed:
			return interface_book
		else:
			return None

	def _get_fitness_value(self, values):

		if len(values) == 0:
			return {'fromObjId':'', 'fromurl':'', 'spider':'', 'content':'', 'fitness': 0}
		elif len(values) == 1:
			return values[0]
		else:
			
			max_index = 0
			max_fitness = values[0]['fitness']

			same_fitness_flag = True
			same_fitness = values[0]['fitness']
			for i, value in enumerate(values):
				if same_fitness != value['fitness']:
					same_fitness_flag = False

				if value['fitness'] > max_fitness:
					max_fitness = value['fitness']
					max_index = i

			if same_fitness_flag:
				# first find doubanBookSpider item
				find_flag = False
				for i, value in enumerate(values):
					if value['spider'] == 'doubanSpider':
						max_index = i
						find_flag = True
						break
				# choose the max length of content
				if not find_flag:
					max_content_length = 0
					for i, value in enumerate(values):
						if type(value['content']) == type(''):
							content_length = len(value['content'])
						elif type(value['content'] == type('')):
							content_length = len(''.join(value['content']))
						else:
							print 'unknow value content type'

						if content_length >= max_content_length:
							max_content_length = content_length
							max_index = i
			
			return values[max_index]

	def _get_download_values(self, values):

		if not values or len(values) == 0:
			return []
		elif len(values) == 1:
			return [values[0]]
		else:
			sorted_values = sorted(values, key=lambda value: value['fitness'], reverse=True)
			return sorted_values

	def _get_face_value(self, values):
		if len(values) == 0:
			return {'fromObjId':'', 'fromurl':'', 'spider':'', 'localpath':'', 'downloadurl':'', 'fitness':0}
		elif len(values) == 1:
			return values[0]
		else:
			
			max_index = 0
			max_fitness = values[0]['fitness']

			same_fitness_flag = True
			same_fitness = values[0]['fitness']
			for i, value in enumerate(values):
				if same_fitness != value['fitness']:
					same_fitness_flag = False

				if value['fitness'] > max_fitness:
					max_fitness = value['fitness']
					max_index = i

			if same_fitness_flag:
				# first find doubanBookSpider item
				find_flag = False
				for i, value in enumerate(values):
					if value['spider'] == 'doubanSpider':
						max_index = i
						find_flag = True
						break
				# choose the fitness size
				if not find_flag:
					max_index = 1
			
			return values[max_index]

	def _get_category_value(self, values):
		category = {'content': '', 'fitness': 0}
		if len(values) == 0:
			return category
		elif len(values) == 0:
			category['content'] = '|' + values[0]['content'].replace('/', '|') + '|'
			return category
		else:
			content = '|'.join(map(lambda x : x['content'].replace('/', '|'), values))
			unique_categories = list(set(content.split('|')))
			category['content'] = '|' + '|'.join(unique_categories) + '|'
			return category
			



def do_clean():
	print '\ncleaner start up'
	
	cleaner = Cleaner()
	cleaner.do_clean()
	#time.sleep(10)

	print 'cleaner end up'


if __name__ == "__main__":
	#print 'hello world start'

	do_clean()

	#print 'hello word end'