#!/usr/bin/env python
# -*- coding: utf-8 -*-




class TimerTask(object):
	def __init__(self, time_interval, completed_func, work_func):
		self.running = False
		self.time_interval = time_interval
		self.completed = completed_func
		self.work = work_func

	def run(self):
		if self.running:
			return

		from twisted.internet import threads
		self.running = True
		d = threads.deferToThread(self.work)
		d.addCallback(self.run_completed) 

	def run_completed(self, result):
		self.running = False
		if self.completed:
			self.completed(result)

	def start(self):
		from twisted.internet import task
		loop = task.LoopingCall(self.run)
		loop.start(self.time_interval) 



if __name__ == '__main__':

	def work():
		#print '\nwork'
		return 'work'

	def completed(result):
		print 'compeled:',result

	def work_2():
		#print '\nwork_2'
		return 'work_2'

	def completed_2(result):
		print 'compeled_2:',result


	from twisted.internet import reactor

	tryTask_1 = TimerTask(11, completed, work)
	tryTask_1.start()

	tryTask_2 = TimerTask(11, completed_2, work_2)
	tryTask_2.start()


	reactor.run()

