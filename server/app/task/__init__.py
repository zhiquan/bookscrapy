# -*- coding: utf-8 -*-


# Declare top-level shortcuts
__all__ = ['do_clean', 'do_check', 'do_statistic', 'TimerTask']
from cleaner import do_clean
from checker import do_check
from statistic import do_statistic
from timerTask import TimerTask