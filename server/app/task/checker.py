#!/usr/bin/env python
# -*- coding: utf-8 -*-


import datetime
import urllib2
import config
from proxy import spiderProxy, proxyProxy
from util.crawler_process import crawler_process

class Checker(object):
	
	def do_check(self):

		self.do_spider_check()
		self.do_proxy_check()



	def do_spider_check(self):
		for spider in spiderProxy.list():
			if spider['name'] == 'proxySpider':
				if spider['status'] == 'closed':
					self._check_closed_spider(spider, config.proxy_crawl_interval)
			else:
				if  spider['status'] == 'idle':
					self._check_idle_spider(spider, config.spider_idle_interval)

				elif spider['status'] == 'closed':
					self._check_closed_spider(spider, config.crawl_interval)

				else:
					print 'spider ', spider['name'], 'keep in status'

	def _check_closed_spider(self, spider, interval):
		closed_time = datetime.datetime.strptime(spider['close_time'], '%Y-%m-%d %H:%M:%S')
		if datetime.datetime.now() - closed_time > datetime.timedelta(seconds=interval):
			crawler_process.crawl(spider['name'])
			#print 'spider ', spider['name'], 'start'

	def _check_idle_spider(self, spider, interval):
		last_idle_time = datetime.datetime.strptime(spider['last_idle_time'],'%Y-%m-%d %H:%M:%S')
		idle_count = int(spider['idle_count'])
		
		seconds = idle_count * interval
		#print last_idle_time, idle_count, seconds, datetime.datetime.now()
		if datetime.datetime.now() - last_idle_time > datetime.timedelta(seconds = seconds):
			crawler_process.resume_crawl(spider['name'])
			#print 'spider ', spider['name'], 'resume'

	def do_proxy_check(self):
		proxys = proxyProxy.list()
		for proxy in proxys:
			proxy_host = "http://" + proxy['address'] + ":" + proxy['port']
			#print '\n', proxy_host, proxy
			url = "http://ip.chinaz.com/getip.aspx"
			try:
				#handlers = [urllib2.ProxyHandler({'http': proxy_temp})]
				#opener = urllib2.build_opener(*handlers)
				#response = opener.open(urllib2.quote(url, safe=":/"), timeout=10)
				
				proxyHandler = urllib2.ProxyHandler({'http': proxy_host})
				opener = urllib2.build_opener(proxyHandler)
				urllib2.install_opener(opener)
				response = urllib2.urlopen(url, timeout=3)


				#urllib2.ProxyHandler(proxy_temp)
				#res = urllib2.urlopen(url,timeout=2).read()
				#print 'res',response.read()
				proxy['status'] = 'work'
				proxyProxy.update(proxy)

			except Exception,e:
				#print 'error:', proxy,e
				proxyProxy.delete_by_id(proxy['_id'])
				#proxy['status'] = 'unwork'
				#proxyProxy.update(proxy)
				continue


def do_check():
	print '\nchecker start up'
	
	checker = Checker()
	checker.do_check()
	#time.sleep(10)

	print 'checker end up'


if __name__ == '__main__':
	do_check()