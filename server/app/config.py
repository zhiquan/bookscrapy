# -*- coding: utf-8 -*-


db_uri = 'mongodb://localhost:27017/'
db_name = 'BookScrapy'

db_col_name_central_book = 'central_book'
db_col_name_clean_task = 'clean_task'
db_col_name_stat_category = 'stat_category'
db_col_name_stat_author = 'stat_author'
db_col_name_proxy_ip = 'proxy_ip'

db_col_name_interface_book = 'interface_book'
db_col_name_category = 'categories'
db_col_name_author = 'authors'

port = 8000
host = '0.0.0.0'

clean_interval = 60*60
check_interval = 4*60
proxy_crawl_interval = 2*60
spider_idle_interval = 10*60
crawl_interval = 60*60*24*2
statistic_interval = 60


book_per_page_count = 25
