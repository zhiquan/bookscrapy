# -*- coding: utf-8 -*-

# Scrapy settings for crawler project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'crawler'

SPIDER_MODULES = ['crawler.spiders']
NEWSPIDER_MODULE = 'crawler.spiders'

LOG_FILE = 'crawler.log'
LOG_LEVEL = 'INFO'
#LOG_LEVEL = 'ERROR'


#scrapyjs setting
SPLASH_URL = 'http://192.168.99.100:8050/'
DUPEFILTER_CLASS = 'scrapyjs.SplashAwareDupeFilter'
HTTPCACHE_STORAGE = 'scrapyjs.SplashAwareFSCacheStorage'

IMAGES_STORE = '../../storage'
FILES_STORE = '../../storage'


#RETRY_ENABLED = True
#RETRY_TIMES = 1
#RETRY_HTTP_CODES = [500, 502, 503, 504, 400, 408]

#REDIRECT_ENABLED = False
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'crawler'

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS=32

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY=2
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN=16
#CONCURRENT_REQUESTS_PER_IP=16

# Disable cookies (enabled by default)
#COOKIES_ENABLED=True

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED=True
#TELNETCONSOLE_PORT=8001

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'crawler.middlewares.MyCustomSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
#   'crawler.middlewares.MyCustomDownloaderMiddleware': 543,

    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware' : None,
    'crawler.middlewares.RotateUserAgentMiddleware' :700,

    #'scrapy.contrib.downloadermiddleware.retry.RetryMiddleware': 500,
    #'crawler.middlewares.IdleRetryMiddleware': 500,

    'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 610,
    #'crawler.middlewares.ProxyMiddleware': 600,
    'crawler.middlewares.HttpProxyMiddleware' : 600,

    #'crawler.middlewares.FilterRequestMiddleware' : 700,

    'scrapyjs.SplashMiddleware': 725
}

# Enable or disable extensions
# See http://scrapy.readthedocs.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'crawler.pipelines.broadcastPipeline' : 298,
    'crawler.pipelines.proxyPipeline' : 299,
    'crawler.pipelines.filterItemPipline' : 300,
    'crawler.pipelines.coverImagesPipeline': 301,
    'crawler.pipelines.ePubBooksPipeline': 302,
    'crawler.pipelines.integrateItemPipeline' : 304,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See http://doc.scrapy.org/en/latest/topics/autothrottle.html
# NOTE: AutoThrottle will honour the standard settings for concurrency and delay
#AUTOTHROTTLE_ENABLED=True
# The initial download delay
#AUTOTHROTTLE_START_DELAY=5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY=60
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG=False

# Enable and configure HTTP caching (disabled by default)
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED=True
#HTTPCACHE_EXPIRATION_SECS=0
#HTTPCACHE_DIR='httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES=[]
#HTTPCACHE_STORAGE='scrapy.extensions.httpcache.FilesystemCacheStorage'
