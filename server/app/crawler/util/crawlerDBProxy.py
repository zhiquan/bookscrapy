# -*- coding: utf-8 -*-

from crawler.items import BookItem, DoubanBookItem


class CawlerDBProxy(object):    
    def __init__(self):
        pass
    #保存
    def save(self,collection,item):
        tmp = collection.find_one({"fromurl":item['fromurl']})
        if tmp is None:
            collection.insert_one(dict(item))


    def haodooBookItemWithFromurl(self, fromurl):
        return self.haodoo_book_col.find_one({"fromurl":fromurl})

    def kindle114BookItemWithFromurl(self, fromurl):
        return self.kindle114_book_col.find_one({"fromurl":fromurl})

    def inienBookItemWithFromurl(self, fromurl):
        return self.inien_book_col.find_one({"fromurl":fromurl})

    def twoepubBookItemWithFromurl(self, fromurl):
        return self.twoepub_book_col.find_one({"fromurl":fromurl})

    def doubanBookItemWithFromurl(self, fromurl):
        return self.douban_book_col.find_one({"fromurl":fromurl})




    def saveKindle114BookItem(self, item):
        if isinstance(item, BookItem):
            self.save(self.kindle114_book_col,item)

    def saveHaodooBookItem(self, item):
        if isinstance(item, BookItem):
            self.save(self.haodoo_book_col,item)

    def saveInienBookItem(self, item):
        if isinstance(item, BookItem):
            self.save(self.inien_book_col, item)

    def saveTwoepubBookItem(self, item):
        if isinstance(item, BookItem):
            self.save(self.twoepub_book_col, item)

    def saveDoubanBookItem(self, item):
        if isinstance(item, DoubanBookItem):
            self.save(self.douban_book_col, item)



    def bookItemWithFromurl(self, fromurl):
        item = self.haodooBookItemWithFromurl(fromurl)
        
        if not item:
            item = self.kindle114BookItemWithFromurl(fromurl)

        if not item:
            item = self.inienBookItemWithFromurl(fromurl)

        if not item:
            item = self.twoepubBookItemWithFromurl(fromurl)

        if not item:
            item = self.doubanBookItemWithFromurl(fromurl)

        return item
        
    def collectionWithFromhost(self, fromhost):
        if cmp(fromhost, 'http://www.haodoo.net/') == 0:
            return self.haodoo_book_col
        elif cmp(fromhost, 'http://www.kindle114.com/') == 0:
            return self.kindle114_book_col
        elif cmp(fromhost, 'http://www.inien.com/') == 0:
            return self.inien_book_col
        elif cmp(fromhost, 'http://www.2epub.net/') == 0:
            return self.twoepub_book_col
        elif cmp(fromhost, 'http://book.douban.com/') == 0:
            return self.douban_book_col
        else:
            return None


if __name__ == '__main__':

    print dir(CawlerDBProxy)
    print 'hello world'