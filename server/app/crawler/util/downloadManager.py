# -*- coding: utf-8 -*-

import urllib2
import os


from mongoDBManager import MongoDBManager



class BookItemDownloadManager(object):
	dbManager = MongoDBManager()

	def __init__(self,item):
		self.storageDir = '../storage/'
		self.item = BookItemDownloadManager.dbManager.bookItemWithFromurl(item['fromurl'])
		self.collection = BookItemDownloadManager.dbManager.collectionWithFromhost(item['fromhost'])

	def downloadBookItem(self, cookies):
		try:
			if len(self.item['bookstoragepath']) != 0:
				print 'it have downloaded bookitem'
				return
			book_url_paths = []
			for download_item in self.item['downloadurls']:
				request = urllib2.Request(download_item[0])
				if cookies:
					headers = self._requestHeader(cookies)
					for header_key in headers:
						request.add_header(header_key, self.headers[header_key])
				response = urllib2.urlopen(request)
				download_item_fullPath = self._bookItemDir() + '/' + download_item[1]
				book_url_paths.append(download_item_fullPath)
				downloadKB = 0
				with open(download_item_fullPath, 'wb') as f:
					while True:
						buffer = response.read(1024*32)
						downloadKB = downloadKB + len(buffer)/1024
						print 'downloadKB', downloadKB, 'kb', download_item[1]
						if not buffer:
							self.collection.update_one({"_id":self.item['_id']}, {'$set':{'bookstoragepath':book_url_paths}})
							break
						f.write(buffer)
				print 'download book', download_item[1]
		except BaseException, e:
			print 'downloadBookItem', e

	def downloadCoverImage(self):
		try:
			if len(self.item['coverstoragepath']) !=0:
				print 'it have downloaded cover img'
				return

			cover_url_paths = []
			for coverurl in self.item['coverurls']:
				print 'coverurl:', coverurl
				cover_img = urllib2.urlopen(coverurl).read()
				#cover_img_name = self.item['title'] + "." + (coverurl).split('.')[-1]
				cover_img_name = coverurl.split('/')[-1]
				cover_img_path = self._bookItemDir() + '/' + cover_img_name
				cover_url_paths.append(cover_img_path)
				with open(cover_img_path, 'wb') as f:
					f.write(cover_img)
					self.collection.update_one({"_id":self.item['_id']}, {'$set':{'coverstoragepath':cover_url_paths}})
				print 'download Image cover', cover_img_name
		except BaseException, e:
			print 'downloadCoverImage', e

	def _bookItemDir(self):
		book_id = self.item['_id']
		book_title = self.item['title']
		bookItemDir = self.storageDir + book_title+ '_'+ str(book_id)
		if not os.path.exists(bookItemDir):
			os.makedirs(bookItemDir)
		return bookItemDir

	def _requestHeader(self, cookies):
		cookiestr = ''
		for cookie in cookies:
			cookieItem = cookie[u'name'] + u'=' + cookie[u'value'] + u';'
			cookiestr = cookiestr + cookieItem
		cookies = cookiestr[0:-1]
		headers = { 	 'Accept'			: 	'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
				    	 'Accept-Encoding'	:	'gzip, deflate, sdch',
				     	 'Accept-Language' 	: 	'zh-CN,zh;q=0.8,en;q=0.6',
				     	 'Cache-Control' 	: 	'max-age=0',
				     	 'Connection'	 	:  	'keep-alive',
				     	 'Host'			 	:	'www.kindle114.com',
					 	 'User-Agent'	 	:  	'Mozilla/5.0 (Windows NT 5.1; rv:40.0) Gecko/20100101 Firefox/40.0',
					 	 'If-Range:Wed'		:	'09 Dec 2015 14:30:10 GMT',
					 	 'Proxy-Connection' :	'keep-alive',
					 	 'Range'			:	'bytes=3550-3550',
					 	 'cookie'			:	 cookies
				   		}
		return headers



if __name__ == '__main__':
	print "download start"

	#bookItemDownloadManager = BookItemDownloadManager()
	#bookItemDownloadManager.downloadBookCoverImage()
	#bookItemDownloadManager.downloadBookEntity()
	url = 'http://www.kindle114.com/forum.php?mod=attachment&aid=MTE4NTI5fDQ3NjIwMTkzfDE0NDk3MzIzMDd8MjY0OTM4fDU2OTM4&nothumb=yes'
	headers = { 'Accept'			: 	'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
				    'Accept-Encoding'	:	'gzip, deflate, sdch',
				     'Accept-Language' 	: 	'zh-CN,zh;q=0.8,en;q=0.6',
				     'Cache-Control' 	: 	'max-age=0',
				     'Connection'	 	:  	'keep-alive',
				     'Host'			 	:	'www.kindle114.com',
				     'cookie'			:	'FjFc_2132_connect_login=1; FjFc_2132_connect_uin=D5C5A2E98E64BBF2122AC9758392DB10; FjFc_2132_connect_synpost_tip=1; FjFc_2132_nofavfid=1; FjFc_2132_ulastactivity=1419316146%7C0; FjFc_2132_smile=1D1; FjFc_2132_connect_is_bind=1; FjFc_2132_lastrequest=e1dah3o5%2FpaClsXsoGOWF23mFyjqZkKMn4MsNeYmhNSWYr190XTn; fB6c_2132_nofavfid=1; fB6c_2132_saltkey=DLI349w6; fB6c_2132_lastvisit=1449646214; fB6c_2132_pc_size_c=0; fB6c_2132_client_created=1449649842; fB6c_2132_client_token=D5C5A2E98E64BBF2122AC9758392DB10; fB6c_2132_auth=4847jTCFtMvd%2FiK5iWu0edTID8DKGfYkRAeZNCIoOioRSSS7z%2FpWaJHd%2BKEm1YTmuJqs1ifrRbzh4rUVVI8kcIiD%2Bww; fB6c_2132_connect_login=1; fB6c_2132_connect_uin=D5C5A2E98E64BBF2122AC9758392DB10; fB6c_2132_stats_qc_login=3; fB6c_2132_connect_last_report_time=2015-12-10; fB6c_2132_st_p=264938%7C1449719770%7C491974ea9ada332593ef50bdf6498b92; fB6c_2132_viewid=tid_56923; fB6c_2132_ulastactivity=1449719770%7C0; fB6c_2132_lastcheckfeed=264938%7C1449719773; CNZZDATA1000138961=1249932927-1419314092-http%253A%252F%252Fwww.baidu.com%252F%7C1449715747; fB6c_2132_secqaa=19454.d2bd21f7e65df33127; fB6c_2132_smile=1D1; fB6c_2132_lastact=1449734572%09forum.php%09attachment; fB6c_2132_connect_is_bind=1',
					 'User-Agent'	 	:  	'Mozilla/5.0 (Windows NT 5.1; rv:40.0) Gecko/20100101 Firefox/40.0',
					 'If-Range:Wed'		:	'09 Dec 2015 14:30:10 GMT',
					 'Proxy-Connection' :	'keep-alive',
					 'Range'			:	'bytes=3550-3550' 
				   }	
	request = urllib2.Request(url)
	for header_key in headers:
		request.add_header(header_key, headers[header_key])
	#print 'header', request.headers
	response = urllib2.urlopen(request)
	print 'headers:\n', response.headers
	with open('00.png', 'wb') as f:
		while True:
			buffer = response.read(1024*32)
			print 'buffer', len(buffer)
			if not buffer:
				break
			f.write(buffer)
	print "download end"
