# -*- coding: utf-8 -*-


#from datetime import datetime
import sys    
reload(sys)  
sys.setdefaultencoding('utf8')

from bookSpider import BookSpider
#from scrapy.http import Request
from crawler.items import ProxyItem

#v="3";m="4";a="2";l="9";q="0";b="5";i="7";w="6";r="8";c="1";
#v="3";m="4";a="2";l="9";q="0";b="5";i="7";w="6";r="8";c="1";

class ProxySpider(BookSpider):
    name = "proxySpider"
    start_urls = (
        #'http://www.cnproxy.com/proxy1.html',
        #'http://www.cnproxy.com/proxy2.html',
        #'http://www.cnproxy.com/proxy3.html',
        #'http://www.cnproxy.com/proxy4.html',
        #'http://www.cnproxy.com/proxy5.html',
        #'http://www.cnproxy.com/proxy6.html',
        #'http://www.cnproxy.com/proxy7.html',
        #'http://www.cnproxy.com/proxy8.html',
        #'http://www.cnproxy.com/proxy9.html',
        #'http://www.cnproxy.com/proxy10.html',
        #'http://www.cnproxy.com/proxyedu1.html',
        #'http://www.cnproxy.com/proxyedu2.html',
        'http://www.xicidaili.com/',
    )
    host_url = "http://www.cnproxy.com/"

    def parse(self, response):
        print '\nbook_url:', response.url

        items = []

        if response.url.startswith("http://www.cnproxy.com"):
            proxy_list = response.selector.xpath("//*[@id='proxylisttb']/table[3]//tr")
            for i, proxy in enumerate(proxy_list):
                if i == 0:
                    continue
                ip = proxy.xpath("td[1]/text()").extract()
                ip = ip[0] if len(ip)> 0 else ''

                port_str = proxy.xpath("td/script/text()").re('write\(":"+(.*)\)')
                port_str = port_str[0] if len(port_str)> 0 else ''
                port_map = {'v':'3','m':'4','a':'2','l':'9','q':'0','b':'5','i':'7','w':'6','r':'8','c':'1','+':''}
                port = ''.join(map(lambda x : port_map[x], list(port_str)))

                protocol = proxy.xpath("td[2]/text()").extract()
                protocol = protocol[0] if len(protocol)> 0 else ''

                delay = proxy.xpath("td[3]/text()").extract()
                delay = delay[0] if len(delay)> 0 else ''

                location = proxy.xpath("td[4]/text()").extract()
                location = location[0] if len(location)> 0 else '' 

                if protocol != 'http' and protocol != 'HTTP':
                    continue

                proxyItem = ProxyItem()
                proxyItem['address'] = ip
                proxyItem['port'] = port
                proxyItem['protocol'] = protocol
                proxyItem['location'] = location
                proxyItem['status'] = 'init'

                items.append(proxyItem)

                print i, 'cnproxy-info:', ip, port, protocol, delay, location

        elif response.url.startswith("http://www.xicidaili.com"):

            proxy_list = response.selector.xpath("//*[@id='ip_list']//tr")
            for i, proxy in enumerate(proxy_list):
                ip = proxy.xpath("td[2]/text()").extract()
                ip = ip[0] if len(ip)> 0 else ''

                port = proxy.xpath("td[3]/text()").extract()
                port = port[0] if len(port)> 0 else ''

                location = proxy.xpath("td[4]/text()").extract()
                location = location[0] if len(location)> 0 else ''

                protocol = proxy.xpath("td[6]/text()").extract()
                protocol = protocol[0] if len(protocol)> 0 else ''

                if protocol != 'http' and protocol != 'HTTP':
                    continue

                if ip == '' or port == '':
                    continue

                proxyItem = ProxyItem()
                proxyItem['address'] = ip
                proxyItem['port'] = port
                proxyItem['protocol'] = protocol
                proxyItem['location'] = location
                proxyItem['status'] = 'init'

                items.append(proxyItem)
                print i, 'xicidaili-info:', ip, port, protocol, location


        else:
            print 'unknown response url'


        return items

        

