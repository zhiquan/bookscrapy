# -*- coding: utf-8 -*-


from datetime import datetime
#import time
import sys    
reload(sys)  
sys.setdefaultencoding('utf8')

from bookSpider import BookSpider
from scrapy.http import Request
from crawler.util.langconv import *

from crawler.items import BookItem


class Zi5Spider(BookSpider):
    name = "zi5Spider"
    allowed_domains = ["zi5.com"]
    start_urls = (
        'http://book.zi5.me/',
    )
    host_url = "http://book.zi5.me"
    status = 'unready'

    def start_requests(self):
        return [Request(self.start_urls[0], callback=self.parse_nav, errback=self.on_error)]

        '''return [Request(self.start_urls[0], callback=self.parse_nav, errback=self.on_error, meta={
                    'splash' :{
                        'endpoint' : 'render.html',
                        'args': {'wait': 10.0}
                    }
                })]'''

    def parse_nav(self, response):
        print response.url
        nav = response.selector.xpath("//*[@id='navigation-next']/a/@href").extract()
        yield Request(nav, callback=self.parse_nav, errback=self.on_error)
        print nav
        '''yield [Request(nav, callback=self.parse_nav, errback=self.on_error, meta={
                    'splash' :{
                        'endpoint' : 'render.html',
                        'args': {'wait': 10.0}
                    }
                })]'''

        book_page_urls = response.selector.xpath("//div[contains(@class, 'thumb-holder')]/a/@href").extract()
        for book_page_url in book_page_urls:
            print book_page_url
            yield Request(nav, callback=self.parse_book, errback=self.on_error)

            '''yield [Request(book_page_url, callback=self.parse_book, errback=self.on_error, meta={
                        'splash' :{
                            'endpoint' : 'render.html',
                            'args': {'wait': 10.0}
                        }
                    })]'''


    def parse_book(self, response):
        print response.url
        book_title = response.selector.xpath("//div[contains(@class, 'h1-wrapper')]/h1/text()").extract()
        book_title = book_title[0] if len(book_title)> 0 else ''

        book_author = response.selector.xpath("//div[contains(@class, 'post-meta-top')]/div[contains(@class, 'pull-left')]/a[1]/text()").extract()
        book_author = book_author[0] if len(book_author)> 0 else ''

        book_ISBN = response.selector.xpath("//div[contains(@class, 'book')]/div[3]/text()[1]").extract()
        book_ISBN = book_ISBN[0] if len(book_ISBN)> 0 else ''
        book_ISBN = book_ISBN.replace(' ', '').replace('\n', '')


        book_cover_url = response.selector.xpath("//img[contains(@class, 'bookcover')]/@src").extract()
        book_cover_url = self.host_url + book_cover_url[0] if len(book_cover_url)> 0 else ''

        book_synopsis =  ''.join(response.selector.xpath("//div[contains(@class, 'post-content')]/p/text()").extract())
        book_category = '/'.join(response.selector.xpath("//div[contains(@class, 'post-meta-category-tag')]/a/text()").extract())

        book_download_url = response.selector.xpath("//a[contains(@href, '.epub')]/@href").extract()
        book_download_url = book_download_url[0] if len(book_download_url)> 0 else ''


        item = BookItem()
        item['title'] = book_title
        item['author'] = book_author
        item['ISBN'] = book_ISBN
        item['category'] = book_category
        item['coverurls'] = [book_cover_url]
        item['synopsis'] = book_synopsis
        item['downloadurls'] = [[book_download_url, book_title + '.epub']]
        item['fromurl'] = response.url
        item['fromhost'] = self.host_url
        item['changed']  = ''
        if hasattr(self, 'crawl_id'):
            item['crawlerid'] = self.crawl_id

        item['updatetime'] = str(datetime.now())
        item['recordtime'] = str(datetime.now())
        item['coverstoragepath'] = []
        item['bookstoragepath'] = []

        #self._handleBookItem(item)
        msg = '%s, %s, %s, %s' % (self.name, self.count, item['title'], item['author'])
        self.logger.info(msg)
        self.count = self.count + 1
        print self.name, self.count, item['category'], item['title'], item['author'], item['ISBN']

        return item

        

