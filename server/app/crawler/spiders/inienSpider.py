# -*- coding: utf-8 -*-


from datetime import datetime
#import time


from bookSpider import BookSpider
from scrapy.http import Request
from crawler.items import BookItem

class InienSpiderExt(BookSpider):
    name = "inienSpider"
    allowed_domains = ["inien.com"]
    start_urls = (
        'http://www.inien.com/w/#/Index',
    )
    host_url = "http://www.inien.com/"


    def start_requests(self):

        return [Request(self.start_urls[0], callback=self.parse_home_page, errback=self.on_error, meta={
                    'splash' :{
                        'endpoint' : 'render.html',
                        'args': {'wait': 10.0}
                    }
                })]

    def parse_home_page(self, response):
        #print 'parse_home_page', response
        hot_book_urls = response.selector.xpath("//div[@id='stat-table']//a/@href").extract()
        #print 'hot_book_urls:', hot_book_urls
        for hot_book_url in hot_book_urls:
            #print 'hot_book_url:', hot_book_url
            yield Request(self._add_url_scheme(hot_book_url),callback=self.parse_book, errback=self.on_error, meta={
                    'splash' : {'endpoint':'render.html','args': {'wait': 10.0}},
                    'category':'hot',
                    'book_from_url': self._add_url_scheme(hot_book_url)
                })

        #print 'nav_url'
        nav_urls = response.selector.xpath("//*[@id='rich-viewer']/table[1]//a/@href").extract()
        for nav_url in nav_urls:
            #print 'nav_url:', nav_url
            yield Request(self._add_url_scheme(nav_url), callback=self.parse_nav, errback=self.on_error, meta={
                    'splash' : {'endpoint' : 'render.html','args': {'wait': 10.0}}
                })

    def parse_nav(self, response):
        #print response.url
        book_category = response.selector.xpath("//*[@id='rich-viewer']/h1/text()").extract()
        book_category = book_category[0] if len(book_category)> 0 else ''

        book_sub_categories = response.selector.xpath("//*[@id='TOC']/ul//a/@href").extract()
        for book_sub_category in book_sub_categories:
            sub_category = book_sub_category.split('#')[-1]
            book_urls = response.selector.xpath("//*[@id='"+ sub_category + "']/ul//a/@href").extract()
            for book_url in book_urls:
                yield Request(self._add_url_scheme(book_url), callback=self.parse_book, errback=self.on_error, meta={
                        'splash' :{'endpoint':'render.html','args': {'wait': 10.0}},
                        'category' : book_category + '/' + sub_category,
                        'book_from_url': self._add_url_scheme(book_url)
                    })

    def parse_book(self, response):

        #print 'category', response.request.meta['category']

        book_download_url = response.selector.xpath('//*[@id="epub-btn"]/@href').extract()
        book_download_url = book_download_url[0] if len(book_download_url)> 0 else ''
        book_download_url = self._add_url_scheme(book_download_url)

        book_title = response.selector.xpath('//*[@id="rich-viewer"]/h1/text()').extract()
        book_title = book_title[0] if len(book_title)> 0 else ''

        book_author = response.selector.xpath('//*[@id="rich-viewer"]/center/text()').extract()
        book_author = book_author[0] if len(book_author)> 0 else ''

        book_url = response.request.meta['book_from_url']
        book_category = response.request.meta['category']

        book_cover_url = ''
        book_synopsis = ''
        book_from_url = book_url
        book_from_host = self.host_url

        item = BookItem()
        item['title'] = book_title
        item['author'] = book_author
        item['coverurls'] = [book_cover_url]
        item['synopsis'] = book_synopsis
        item['downloadurls'] = [(book_download_url, book_title + '.epub')]
        item['fromurl'] = book_from_url
        item['fromhost'] = book_from_host
        item['category'] = book_category
        item['changed']  = ''
        if hasattr(self, 'crawl_id'):
            item['crawlerid'] = self.crawl_id

        item['updatetime'] = str(datetime.now())
        item['recordtime'] = str(datetime.now())
        item['coverstoragepath'] = []
        item['bookstoragepath'] = []

        self.count = self.count + 1
        msg = '%s, %s, %s, %s' % (self.name, self.count, item['title'], item['author'])
        self.logger.info(msg)
        
        print self.name, self.count, book_author, book_title, book_download_url

        return item

    def _add_url_scheme(self, url):
        scheme_url = "http://www.inien.com/w/" + url
        #print scheme_url
        return scheme_url