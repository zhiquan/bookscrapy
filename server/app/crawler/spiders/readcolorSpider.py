# -*- coding: utf-8 -*-


from datetime import datetime
import sys    
reload(sys)  
sys.setdefaultencoding('utf8')

from bookSpider import BookSpider
from scrapy.http import Request
from crawler.util.langconv import *

from crawler.items import BookItem


class ReadColorSpider(BookSpider):
    name = "readColorSpider"
    allowed_domains = ["readcolor.com"]
    start_urls = (
        'http://readcolor.com/books',
    )
    host_url = "http://readcolor.com"
    #status = 'unready'

    def start_requests(self):
        #print 'start _request:', self.start_urls[0]
        return [Request(self.start_urls[0], callback=self.parse_nav, errback=self.on_error)]
        #return [Request(self.start_urls[0], callback=self.parse_nav)]

    def parse_nav(self, response):
        print 'nav_url:', response.url
        book_urls = response.selector.xpath("//div[contains(@class, 'span4')]/a/@href").extract()
        for book_url in book_urls:
            yield Request(book_url, callback=self.parse_book, errback=self.on_error)

        
        next_page_url = response.selector.xpath("//div[contains(@class, 'pagination')]/ul/li[contains(@class, 'next')]/a/@href").extract()
        next_page_url = next_page_url[0] if len(next_page_url)> 0 else ''
        print 'next_page_url:', next_page_url
        if next_page_url != '':
            yield Request(next_page_url, callback=self.parse_nav, errback=self.on_error)

    def parse_book(self, response):
        print 'book_url:', response.url
        book_title = response.selector.xpath("//div[contains(@class, 'media-body')]/h2/text()").extract()
        book_title = book_title[0] if len(book_title)> 0 else ''

        book_author = response.selector.xpath("//div[contains(@class, 'media-body')]//span[contains(@itemprop, 'author')]/a/text()").extract()
        book_author = book_author[0] if len(book_author)> 0 else ''

        book_cover_url = response.selector.xpath("//*[@id='book-image']/@href").extract()
        book_cover_url = book_cover_url[0] if len(book_cover_url)> 0 else ''

        book_synopsis = response.selector.xpath("//*[@id='book-description']/p[1]/text()").extract()
        book_synopsis = book_synopsis[0] if len(book_synopsis)> 0 else ''

        book_category = response.selector.xpath("//div[contains(@class, 'span3')]/ul[1]/li/a/text()").extract()
        book_category = '/'.join(map(lambda x : x.strip(), book_category))

        book_download_items = []
        book_download_urls = response.selector.xpath("//*[@id='book-resources']//a[contains(@class, 'epub')]/@href").extract()
        for i, book_download_url in enumerate(book_download_urls):
            book_download_items.append([self.host_url + book_download_url, book_title+'_' + str(i) + '.epub'])

        item = BookItem()
        item['title'] = book_title
        item['author'] = book_author
        item['category'] = book_category
        item['coverurls'] = [book_cover_url]
        item['synopsis'] = book_synopsis
        item['downloadurls'] = book_download_items
        item['fromurl'] = response.url
        item['fromhost'] = self.host_url
        item['changed']  = ''
        if hasattr(self, 'crawl_id'):
            item['crawlerid'] = self.crawl_id

        item['updatetime'] = str(datetime.now())
        item['recordtime'] = str(datetime.now())
        item['coverstoragepath'] = []
        item['bookstoragepath'] = []

        #self._handleBookItem(item)
        msg = '%s, %s, %s, %s' % (self.name, self.count, item['title'], item['author'])
        self.logger.info(msg)
        self.count = self.count + 1
        print self.name, self.count, item['category'], item['title'], item['author'], 

        return item

        

