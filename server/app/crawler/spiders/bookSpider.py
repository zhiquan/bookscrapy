# -*- coding: utf-8 -*-

import scrapy
from scrapy.spidermiddlewares.httperror import HttpError
import uuid

class BookSpider(scrapy.Spider):
    status = 'init'
    host_url = ''

    count = 0
    spider_insert_count = 0
    spider_update_count = 0
    spider_drop_count = 0

    central_insert_count = 0
    central_update_count = 0
    central_drop_count = 0

    crawl_id = ''
    start_time = ''
    close_time = ''
    last_idle_time = ''
    last_resume_time = ''

    idle_count = 0
    err_count = 0
    err_info = ''
    proxy = ''

    def __init__(self, *a, **kw):
        super(BookSpider, self).__init__(*a, **kw)
        self.crawl_id = str(uuid.uuid1())

    @staticmethod
    def spider_info(self):
        spider_info = {}
        spider_info['crawl_id'] = self.crawl_id
        spider_info['name'] = self.name
        spider_info['start_time'] = self.start_time
        spider_info['close_time'] = self.close_time
        spider_info['last_idle_time'] = self.last_idle_time
        spider_info['last_resume_time'] = self.last_resume_time
        spider_info['idle_count'] = self.idle_count
        spider_info['status'] = self.status
        spider_info['err_count'] = self.err_count
        spider_info['err_info'] = self.err_info
        spider_info['host_url'] = self.host_url

        spider_info['count'] = self.count
        spider_info['spider_insert_count'] = self.spider_insert_count
        spider_info['spider_update_count'] = self.spider_update_count
        spider_info['spider_drop_count'] = self.spider_drop_count

        spider_info['central_insert_count'] = self.central_insert_count
        spider_info['central_update_count'] = self.central_update_count
        spider_info['central_drop_count'] = self.central_drop_count

        return spider_info

    def on_error(self, failure):
        #print 'failure:', failure
        response = ''
        if isinstance(failure.value, HttpError):
            response = failure.value.response
            #print 'response:', response,response.status
            
        from scrapy import signals
        print self.name, 'send error signal', response, failure
        #how to retry the request
        self.crawler.signals.send_catch_log(signals.spider_error, failure=failure, response=response,spider=self)
        #raise ValueError

        

