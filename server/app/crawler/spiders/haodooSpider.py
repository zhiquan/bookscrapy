# -*- coding: utf-8 -*-


from datetime import datetime
#import time

import sys    
reload(sys)  
sys.setdefaultencoding('utf8')


from bookSpider import BookSpider
from scrapy.http import Request
from crawler.items import BookItem
from crawler.util.langconv import *

#import logging
#logger = logging.getLogger(__name__)
class HaodooSpider(BookSpider):
    name = "haodooSpider"
    allowed_domains = ["haodoo.net"]
    start_urls = (
        'http://www.haodoo.net/',
    )
    host_url = 'http://www.haodoo.net/'


    def start_requests(self):
    	return [Request(self.start_urls[0], callback=self.parse_nav)]

    def parse_nav(self, response):
        navElements = response.selector.xpath("//td[@class='a03']/a")
        for navElement in navElements:
    	   navtype = navElement.xpath('text()').extract()[0]
    	   navurl =  self.start_urls[0] + navElement.xpath('@href').extract()[0]
    	   #print navtype, navurl
    	   yield Request(navurl, meta = {'navtype':navtype}, callback = self.parse_entry)
           #time.sleep(1)
           #break

    def parse_entry(self, response):
    	#print 'hello world', response.request.meta['navtype'], response
    	navtype = response.request.meta['navtype']
    	bookurls = response.selector.xpath("//a[contains(@href, '?M=book&P=')]/@href").extract()
    	
    	#print 'bookcounts', len(bookurls)

    	for bookurl in bookurls:
    	   bookurl = self.start_urls[0] + bookurl
    	   yield Request(bookurl, meta = {'navtype' : navtype})
           #time.sleep(1)

    def parse(self, response):
    	#print response
    	author = response.selector.xpath("//form[@name='Book']/font[1]/text()").extract()
        author = author[0] if len(author)> 0 else ''
        author = Converter('zh-hans').convert(author.decode('utf-8'))
        author = author.encode('utf-8')


    	booktitle = response.selector.xpath("//form[@name='Book']/text()").extract()
        booktitle = booktitle[0] if len(booktitle)> 0 else ''
        booktitle = booktitle.replace(' ', '').replace('《','').replace('》','')
        booktitle = Converter('zh-hans').convert(booktitle.decode('utf-8'))
        booktitle = booktitle.encode('utf-8')
    	#print self.count, author, booktitle
        

    	#downloadepubcode = response.selector.xpath("//form[@name='Book]/input[@href='下載 epub 檔']/@onclick").extract().re(r'DownloadEpub:\s*(.*)')
    	#downloadvepubcode = response.selector.xpath("//form[@name='Book/input[@href='下載直式 epub 檔']/@onclick").extract().re(r'DownloadVEpub:\s*(.*)')
    	downloadepubcode = response.selector.xpath("//form[@name='Book']/input[contains(@onclick, 'DownloadEpub')]/@onclick").extract()
        downloadepubcode = downloadepubcode[0] if len(downloadepubcode)> 0 else ''
        downloadepubcode = downloadepubcode.replace("DownloadEpub('", '').replace("')", '')
        #print downloadepubcode

        
    	downloadvepubcode = response.selector.xpath("//form[@name='Book']/input[contains(@onclick, 'DownloadVEpub')]/@onclick").extract()
    	downloadvepubcode = downloadvepubcode[0] if len(downloadvepubcode)> 0 else ''
        downloadvepubcode = downloadvepubcode.replace("DownloadVEpub('", '').replace("')", '')

    	downloadepuburl = [self._downloadurl(downloadepubcode), booktitle + '.epub']
    	downloadvepuburl = [self._downloadurl(downloadvepubcode), booktitle + '_v.epub']

    	#print downloadepuburl, downloadvepuburl


    	coverurls = response.selector.xpath("//table[@class='m11']/tr/td/img/@src").extract()
    	coverurls = map(lambda coverurl : self.start_urls[0] + coverurl, coverurls)
    	#print coverurls

    	synopsis = "".join(response.selector.xpath("//table[@class='m11']/tr/td/text()").extract())
        synopsis = Converter('zh-hans').convert(synopsis.decode('utf-8'))
        synopsis = synopsis.encode('utf-8')
    	#print synopsis
    	#from_url = self.start_urls[0] + response.url
        from_url = response.url



    	item = BookItem()
    	item['title'] = booktitle
    	item['author'] = author
    	item['coverurls'] = coverurls
    	item['synopsis'] = synopsis
    	item['downloadurls'] = [downloadepuburl, downloadvepuburl]
    	item['fromurl'] = from_url
    	item['fromhost'] = self.start_urls[0]
        item['changed']  = ''
        if hasattr(self, 'crawl_id'):
            item['crawlerid'] = self.crawl_id

    	item['updatetime'] = str(datetime.now())
    	item['recordtime'] = str(datetime.now())
    	item['coverstoragepath'] = []
    	item['bookstoragepath'] = []


        #self._handleBookItem(item)

        #print self.name, self.count, item['title'], item['author']
        #self.log(self.name, self.count, item['title', item['author']])
        self.count = self.count + 1
        msg = '%s, %s, %s, %s, %s' % (self.name, self.count, item['title'], item['author'],item['fromurl'])
        self.logger.info(msg)
        #print HaodooSpider.name
        print msg
        #WSHandler.broadcast_ws_message(msg)

        return item

    #private-method
    def _downloadurl(self, downloadcode):
    	#for example: http://haodoo.net/?M=d&P=A435.epub
    	downloadurl = self.start_urls[0] + '?M=d&P=' + downloadcode + '.epub'
    	return downloadurl
