# -*- coding: utf-8 -*-


from datetime import datetime
#import time

from bookSpider import BookSpider
from scrapy.http import Request
from crawler.items import BookItem


class TwoepubSpider(BookSpider):
    name = "twoEpubSpider"
    allowed_domains = ["2epub.net"]
    start_urls = (
        'http://www.2epub.net',
    )
    host_url = "http://www.2epub.net/"


    def start_requests(self):
    	return [Request(self.start_urls[0], callback=self.parse_nav)]

    def parse_nav(self, response):
        nav_urls = response.selector.xpath("//*[@id='primary']/ul//a/@href").extract()
        for i, nav_url in enumerate(nav_urls):
            #print i, nav_url
            if i == 0: 
                continue
            #yield Request(self.start_urls[0] + nav_url, callback=self.parse_page_url, meta={'nav_index':i})
            if i>=5:
                break


        #parse home page
        first_page_book_urls = self._parse_book_url(response)
        for i, book_url in enumerate(first_page_book_urls):
            if i == 0 or i == 1:
                continue
            book_from_url = self.start_urls[0] + book_url
            yield Request(self.start_urls[0] + book_url, callback = self.parse_book,errback=self.on_error,meta={'book_from_url':book_from_url})
            #time.sleep(1)

        page_urls = self._parse_page_url(response)
        for i, page_url in enumerate(page_urls):
            #print 'home nav', i, 'page', page_url
            yield Request(self.start_urls[0] + page_url, callback=self.parse_book_url,errback=self.on_error,)
            #time.sleep(1)

            #if i>=2:
            #    break

    def parse_page_url(self, response):
        print 'parse_page_url', response.request.meta['nav_index']
        page_urls = self._parse_page_url(response)
        for i, page_url in enumerate(page_urls):
            yield Request(self.start_urls[0] + page_url, callback=self.parse_book_url_ext,errback=self.on_error,)
            #time.sleep(1)
            #if i >= 2:
            #   break
    
    def parse_book_url(self, response):
        page_book_urls = self._parse_book_url(response)
        for book_url in page_book_urls:
            book_from_url = self.start_urls[0] + book_url
            yield Request(book_from_url, callback = self.parse_book,errback=self.on_error,meta={'book_from_url':book_from_url})
            #time.sleep(1)

    def parse_book_url_ext(self, response):
        page_book_urls = self._parse_book_url_ext(response)
        for book_url in page_book_urls:
            book_from_url = self.start_urls[0] + book_url
            yield Request(book_from_url, callback = self.parse_book, errback=self.on_error,meta={'book_from_url':book_from_url})
            #time.sleep(1)

    def parse_book(self, response):

        book_title = response.selector.xpath("//*[@id='page-title']/text()").extract()
        book_title = book_title[0] if len(book_title)> 0 else ''

        book_author = response.selector.xpath("//div[@class='content']/div[contains(@class,'field-field-author')]/div/div").extract()
        book_author = book_author[0] if len(book_author)> 0 else ''
        book_author = book_author.split('</div>')[-2] if len(book_author.split('</div>'))>=2 else ''
        book_author = book_author.replace(' ', '').replace('\n', '')
        #print book_author

        book_synopsis = ''.join(response.selector.xpath("//div[@class='content']/p/text()").extract())

        book_cover = response.selector.xpath("//div[@class='content']/div[contains(@class, 'field-field-cover')]/div/div/img/@src").extract()
        book_cover = book_cover[0] if len(book_cover)> 0 else ''
        book_cover = (self.start_urls[0] + book_cover) if cmp(book_cover, '') != 0 else ''

        book_download = response.selector.xpath("//*[@id='attachments']/tbody/tr/td/a[contains(@href, '.epub')]/@href").extract()
        book_download = book_download[0] if len(book_download)> 0 else ''


        #print '\ntitle:', book_title, 'author:', 'cover:', book_cover, 'download:',book_download

        item = BookItem()
        item['title'] = book_title
        item['author'] = book_author
        item['coverurls'] = [book_cover]
        item['synopsis'] = book_synopsis
        item['downloadurls'] = [[book_download, book_title + '.epub']]
        item['fromurl'] = response.request.meta['book_from_url']
        item['fromhost'] = self.host_url
        item['changed']  = ''
        if hasattr(self, 'crawl_id'):
            item['crawlerid'] = self.crawl_id


        item['updatetime'] = str(datetime.now())
        item['recordtime'] = str(datetime.now())
        item['coverstoragepath'] = []
        item['bookstoragepath'] = []

        self.count = self.count + 1
        msg = '%s, %s, %s, %s' % (self.name, self.count, item['title'], item['author'])
        self.logger.info(msg)
        
        print self.name, self.count, book_author, book_title, book_download

        return item



    def _parse_page_url(self, response):
        page_tmplate = ''
        page_urls = response.selector.xpath("//div[@class='item-list']/ul[@class='pager']//a/@href").extract()
        page_tmplate = page_urls[0][0:-1]
        page_count = page_urls[-1].split('=')[-1]
        #print 'page_tmplate:', page_tmplate, 'page_count:', page_count
        total_page_urls = []
        for i in xrange(0, int(page_count)):
            page_url = page_tmplate + str(i)
            total_page_urls.append(page_url)

        return total_page_urls

    def _parse_book_url(self, response):
        book_from_urls = response.selector.xpath("//*[@id='main-content']/div[contains(@id, 'node-')]/h2/a/@href").extract()
        return book_from_urls

    def _parse_book_url_ext(self, response):
        book_from_urls = response.selector.xpath("//*[@id='main-content']/div[contains(@class, 'col_2')]/a/@href").extract()
        return book_from_urls
        


