# -*- coding: utf-8 -*-

import time
from datetime import datetime


from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

from bookSpider import BookSpider
from scrapy.http import Request
from crawler.items import BookItem


class Kindle114Spider(BookSpider):
	name = "kindle114Spider"
	allowned_domain = ["kindle114.com"]
	start_urls = []
	driver = None
	host_url = "http://www.kindle114.com/"
	status = 'unready'

	def __init__(self):
		self.driver = webdriver.Firefox()
		#self.driver = webdriver.Chrome()
		self.user = ""
		self.password = ""
		self.login_url = "http://www.kindle114.com/member.php?mod=logging&action=login"
		self.i = 0

	def closed(self, reason):
		self.driver.close()

	def start_requests(self):
		username = '331304595'
		password = 'jinzhu'
		self.login_with_qq(username, password)

		entries = ['http://www.kindle114.com/forum-2-1.html', 	#kindle电子书mobi,epub,pdf下载
				   'http://www.kindle114.com/forum-36-1.html',  #kindle漫画mobi,epub,pdf下载
				   'http://www.kindle114.com/forum-50-1.html'	#kindle电子杂志
				   ]
		book_page_urls = []
		for entry in entries:
			self.driver.get(entry)
			book_page_urls.append(entry)
			book_pages = self.driver.find_element_by_xpath("//div[@id='pgt']/span/div/a[@class='last']").text
			book_pages = book_pages.replace('.', '').replace(' ', '')
			book_page_url_template  = self.driver.find_element_by_link_text('2').get_attribute('href')[0:-1]
			#print book_page_url_template
			for book_page in range(2, int(book_pages)+1):
				book_page_url = book_page_url_template + str(book_page)
				book_page_urls.append(book_page_url)
				#print book_page_url
				#self.logger.info(book_page_url)

		for book_page_url in book_page_urls:
			yield Request(book_page_url)

	def parse(self, response):
		#print response.url
		self.logger.info(response.url)
		book_elements = response.selector.xpath("//a[@class='xst']")
		for i, book_element in enumerate(book_elements):
			book_url = book_element.xpath("@href").extract()[0]
			if not book_url.startswith('http://www.kindle114.com/'):
				book_url = 'http://www.kindle114.com/' + book_url
			#book_title = book_element.xpath("text()").extract()[0]
			#print i, book_title, book_url
			item = self.parse_book(book_url)
			yield item
			self.driver.implicitly_wait(1) # seconds
			#time.sleep(1)

	def parse_book(self, book_url):

		self.driver.get(book_url)
		#print book_url
		try:
			book_title = self.driver.find_element_by_xpath("//a[@id='thread_subject']").text
			book_author = self.driver.find_element_by_xpath("//div[@class='pct']/div[@class='pcb']/table/tbody/tr/td/a/span/u").text
			book_cover_url = self.driver.find_element_by_xpath("//div[@class='pct']/div[@class='pcb']/table/tbody/tr/td/img[@class='JIATHIS_IMG_OK']").get_attribute('src')

			#contain baidu download
			book_synopsis = self.driver.find_element_by_xpath("//td[contains(@id, 'postmessage_')]").text
			book_download_url_elements = self.driver.find_elements_by_xpath("//div[@class='t_fsz']/div[@class='pattl']/ignore_js_op/dl/dd/p/a[contains(@id, 'aid')]")
		except NoSuchElementException:
			book_download_url_elements = None

		book_download_urls = []
		if bool(book_download_url_elements):
			for book_download_url_element in book_download_url_elements:
				book_download_url = book_download_url_element.get_attribute('href')
				book_download_name = book_download_url_element.text
				book_download_urls.append((book_download_url, book_download_name))
				#print 'book_download_url_elements.text:', book_download_url_element.text, book_download_url_element.get_attribute('href')
				#book_download_urls.append(book_download_url)
				#book_download_url_element.click()

		print self.i, book_url, book_title
		#print self.i, "book_url:", book_url, "book_title:",  book_title, "book_author:", book_author,\
		#	  "book_cover_url:", book_cover_url, "book_download_urls:", book_download_urls

		#self.logger.info("%d, %s, %s, %s, %s, %s", self.i, book_url, book_title, book_author,book_cover_url, book_download_urls)
		self.logger.info("%d, %s", self.i, book_url)
		self.i = self.i + 1

		item = BookItem()
		item['title'] = book_title
		item['author'] = book_author
		item['coverurls'] = [book_cover_url]
		item['synopsis'] = book_synopsis
		item['downloadurls'] = book_download_urls
		item['fromurl'] = book_url
		item['fromhost'] = "http://wwww.kindle114.com"
		item['changed']  = ''
		if hasattr(self, 'crawl_id'):
			item['crawlerid'] = self.crawl_id
		item['updatetime'] = str(datetime.now())
		item['recordtime'] = str(datetime.now())
		item['coverstoragepath'] = []
		item['bookstoragepath'] = []

		#self.handleBookItem(item)

		return item

	def login_with_qq(self, un, ps):

		self.driver.get(self.login_url)
		self.driver.find_element_by_xpath("//div[@class='mb_3']/div/p/a").click()
		time.sleep(10)

		self.driver.switch_to.frame('ptlogin_iframe')
		#switcher_plogin = self.driver.find_element_by_id('switcher_plogin').text
		self.driver.find_element_by_id('switcher_plogin').click()

		username = self.driver.find_element_by_name('u')
		username.clear()
		username.send_keys(un)

		password = self.driver.find_element_by_name('p')
		password.clear()
		password.send_keys(ps)

		self.driver.find_element_by_id('login_button').click()
		time.sleep(10)


if __name__ == 'main':
	print 'hello world'




