#!/usr/bin/python
#-*-coding:utf-8-*-

import random
from proxy import proxyProxy
from util.crawler_process import crawler_process


import logging
logger = logging.getLogger(__name__)


class ProxyMiddleware(object):

	# PROXIES = proxyProxy.list_valid_proxy()
	PROXIES = proxyProxy.list()
	PROXIES.append({'address': '127.0.0.1', 'port': '80'})

	def process_request(self, request, spider):
        # TODO implement complex proxy providing algorithm
		# msg = '%s, %s,%s' % (request.url, spider.name, request.meta)
		# logger.info(msg)

		if self.use_proxy(request, spider):
			# print self.PROXIES
			if spider.proxy == 'clear':
				request.meta['retry_proxy'] = {}

			retry_proxy = request.meta.get('retry_proxy', {})
			proxy_str = self.unused_proxy(retry_proxy)
			#print request.url, 'Proxy:proxy', proxy_str
			#print proxy_str, proxy_str is None
			
			if proxy_str is None:
				spider.proxy = 'clear'
				crawler_process.pause_crawl(spider.name)
			elif proxy_str == 'http://127.0.0.1:80':
				print 'do not use proxy'
				del request.meta['proxy']
			else:
				request.meta['proxy'] = proxy_str

	def use_proxy(self, request, spider):
	    """
	    using direct download for depth <= 2
	    using proxy with probability 0.3
	    """
	    # if "depth" in request.meta and int(request.meta['depth']) <= 2:
	    #    return False
	    # i = random.randint(1, 10)
	    # return i <= 2
	    # return True
	    if spider.name == 'doubanSpider':
	    	return True
	    	# return False
	    else:
	    	return False

	def unused_proxy(self, retry_proxy):
		used_proxys = retry_proxy.keys()

		proxys = map( lambda x : "http://%s:%s" % (x['address'], x['port']), self.PROXIES)

		un_unsed_proxys = list( set(proxys) - set(used_proxys) )

		if len(un_unsed_proxys) == 0:
			return None
		else:
			proxy_str = random.choice(un_unsed_proxys)
			return proxy_str

