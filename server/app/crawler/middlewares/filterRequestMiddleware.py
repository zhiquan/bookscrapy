#!/usr/bin/python
#-*-coding:utf-8-*-


#import scrapy
import logging
logger = logging.getLogger(__name__)

class FilterRequestMiddleware(object):
    
    def process_request(self, request, spider):
    	#print 'request.meta:', request.meta
    	#msg = '%s, %s' % (request.url, request.meta)
    	proxy = request.meta.get('proxy', '')
        dont_redirect = request.meta.get('dont_redirect', '')
    	print request.url, 'proxy:', proxy,'dont_redirect:', dont_redirect
        logger.info("%s, %s, %s" % (request.url, request.headers, dont_redirect))
        #if spider.name == 'doubanSpider' and request.url.startswith('https://www.douban.com/misc/sorry'):
        #	logger.info(request.url)
        #	raise scrapy.exceptions.IgnoreRequest

