from scrapy.contrib.downloadermiddleware.retry import RetryMiddleware
from scrapy import log


import logging
logger = logging.getLogger(__name__)

class IdleRetryMiddleware(RetryMiddleware):

    def _retry(self, request, reason, spider):
        retries = request.meta.get('retry_times', 0) + 1
        
        msg = '%s, %s, %s, %s, %s' % (request.url, reason, spider.name,retries, request.meta)
        logger.info(msg)


        reason_msg = '%s' % (reason)

        logger.info(reason_msg)

        if reason_msg.startswith("User timeout caused connection failure") or reason_msg.startswith("An error occurred while connecting") or \
            reason_msg.startswith("TCP connection timed out") or reason_msg.startswith("Connection was refused by other side"):
            retryreq = request.copy()
            proxy = request.meta.get('proxy', '127.0.0.1:80')
            retryreq.meta.setdefault('retry_proxy', {})[proxy] = True

            retryreq.meta['retry_times'] = retries
            retryreq.dont_filter = True
            retryreq.priority = request.priority + self.priority_adjust

            msg = '%s, %s, %s, %s, %s' % (retryreq.url, reason, spider.name, retries, retryreq.meta)
            logger.info(msg)

            return retryreq

        if type(reason) == type('') and reason.split(' ')[0] == '403':
            #msg = 'idle Retry middleware spider pause:%s' % retries
            #msg = '%s, %s, %s, %s, %s' % (request.url, reason, spider,retries, request.meta)
            #print msg
            #logger.info(msg)

            #from util.crawler_process import crawler_process
            #if crawler_process.is_working(spider.name):
            #    crawler_process.pause_crawl(spider.name)
            retryreq = request.copy()
            proxy = request.meta.get('proxy', '127.0.0.1:80')
            retryreq.meta.setdefault('retry_proxy', {})[proxy] = True

            retryreq.meta['retry_times'] = retries
            retryreq.dont_filter = True
            retryreq.priority = request.priority + self.priority_adjust

            msg = '%s, %s, %s, %s, %s' % (retryreq.url, reason, spider.name, retries, retryreq.meta)
            logger.info(msg)

            return retryreq

        if retries <= self.max_retry_times:
            log.msg(format="Retrying %(request)s (failed %(retries)d times): %(reason)s",
                    level=log.DEBUG, spider=spider, request=request, retries=retries, reason=reason)
            retryreq = request.copy()
            retryreq.meta['retry_times'] = retries
            retryreq.dont_filter = True
            retryreq.priority = request.priority + self.priority_adjust
            return retryreq
        else:
            # do something with the request: inspect request.meta, look at request.url...
            log.msg(format="Gave up retrying %(request)s (failed %(retries)d times): %(reason)s",
                    level=log.DEBUG, spider=spider, request=request, retries=retries, reason=reason)