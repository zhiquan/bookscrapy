# Declare top-level shortcuts

__all__ = ['RotateUserAgentMiddleware', 'IdleRetryMiddleware', 'ProxyMiddleware', 'FilterRequestMiddleware', 'HttpProxyMiddleware']

from crawler.middlewares.rotateUserAgentMiddleware import RotateUserAgentMiddleware
from crawler.middlewares.idleRetryMiddleware import IdleRetryMiddleware
from crawler.middlewares.proxyMiddleware import ProxyMiddleware
from crawler.middlewares.filterRequestMiddleware import FilterRequestMiddleware
from crawler.middlewares.HttpProxyMiddleware import HttpProxyMiddleware