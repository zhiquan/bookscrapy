#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
from datetime import datetime, timedelta
import random
from twisted.web._newclient import ResponseNeverReceived
from twisted.internet.error import TimeoutError, ConnectionRefusedError, ConnectError
#import fetch_free_proxyes

from proxy import proxyProxy

logger = logging.getLogger(__name__)

class HttpProxyMiddleware(object):
    # 遇到这些类型的错误直接当做代理不可用处理掉, 不再传给retrymiddleware
    DONT_RETRY_ERRORS = (TimeoutError, ConnectionRefusedError, ResponseNeverReceived, ConnectError, ValueError)
    
    def __init__(self, settings):
        # 保存上次不用代理直接连接的时间点
        self.last_no_proxy_time = datetime.now()
        # 一定分钟数后切换回不用代理, 因为用代理影响到速度
        self.recover_interval = 20
        self.direct_proxyes = [{"proxy": None}]
        self.proxy_status = {}
        self.direct_count = 0
        self.cycle_count = 3

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings)

    def valid_proxy(self):
        return map(lambda x : { "proxy":"http://" + x['address'] + ":" + x['port']}, proxyProxy.list_valid_proxy() )

    def get_valid_proxy(self, spider, policy):

        if datetime.now() > (self.last_no_proxy_time + timedelta(minutes=self.recover_interval)):
            logger.info("After %d minutes later, recover from using proxy" % self.recover_interval)
            self.last_no_proxy_time = datetime.now()
            self.proxy_status.setdefault(str(None), 0)
            self.proxy_status[str(None)] = 0
            return None
        
        proxy = self.direct_proxyes[0]['proxy']

        self.direct_count = self.direct_count + 1

        if self.proxy_status.setdefault(str(None), 0) > 0 or self.direct_count % self.cycle_count == 0:
            #proxy = random.choice(self.valid_proxy())['proxy']
            proxy = self.get_anthor_valid_proxy(spider,proxy)

        return proxy

    def get_anthor_valid_proxy(self, spider, proxy):

        new_proxy = None
        invalid_proxyes = []
        for key in self.proxy_status:
            count = self.proxy_status[key]
            if count > 0:
                invalid_proxyes.append(key)

        valid_proxyes = []
        valid_proxyes.extend(map( lambda x : x['proxy'], self.valid_proxy() ))
        valid_proxyes.append(str(None))
        #print 'valid_proxyes:', valid_proxyes, 'invalid_proxyes:', invalid_proxyes
        un_unsed_proxyes = list( set(valid_proxyes) - set(invalid_proxyes))
        if len(un_unsed_proxyes) == 0:
            # stop crawl
            from util.crawler_process import crawler_process
            if crawler_process.is_working(spider.name):
                crawler_process.pause_crawl(spider.name)
                #clear proxy_status
                self.proxy_status = {}
        else:
            new_proxy = random.choice(un_unsed_proxyes)

        if new_proxy == str(None):
            new_proxy = None

        return new_proxy

    def set_proxy(self, request, proxy):
        if proxy:
            request.meta['proxy'] = proxy
        elif 'proxy' in request.meta.keys():
            del request.meta['proxy']

    def use_proxy(self, request, spider):
        """
        using direct download for depth <= 2
        using proxy with probability 0.3
        """
        # if "depth" in request.meta and int(request.meta['depth']) <= 2:
        #    return False
        # i = random.randint(1, 10)
        # return i <= 2
        # return True
        if spider.name == 'doubanSpider':
            return True
            # return False
        else:
            return False


    def process_request(self, request, spider):
        """
        将request设置为使用代理
        """

        if self.use_proxy(request, spider):
            proxy = self.get_valid_proxy(spider, None)
            self.set_proxy(request, proxy)
            request.meta["dont_redirect"] = True  # 有些代理会把请求重定向到一个莫名其妙的地址

                    
    def process_response(self, request, response, spider):
        """
        检查response, 根据内容切换到下一个proxy, 或者禁用proxy, 或者什么都不做
        """
        # 跳过出现这种异常code的代理 #response.status != 200 更直接一点
        #if response.status in [500, 502, 503, 504, 403, 400, 401, 409, 301, 302, 407]:
        if spider.name != 'doubanSpider':
            return response

        logger.info('response:%s, %s, %s, %s' % (request.url,response, spider.name, request.meta.get('proxy', '')))
        if response.status in [500, 502, 503, 504, 403, 400, 401, 409, 301, 302, 407]:
            new_request = request.copy()
            if 'Cookie' in new_request.headers.keys():
                del new_request.headers['Cookie']
            new_request.dont_filter = True
            current_proxy = request.meta.get('proxy', None)
            
            invalid_count = self.proxy_status.setdefault(str(current_proxy), 0)
            self.proxy_status[str(current_proxy)] = invalid_count + 1

            new_proxy = self.get_anthor_valid_proxy(spider, current_proxy)
            self.set_proxy(new_request, new_proxy)
            return new_request
        else:
            return response

    def process_exception(self, request, exception, spider):
        """
        处理由于使用代理导致的连接异常
        """
        # 只有当proxy_index>fixed_proxy-1时才进行比较, 这样能保证至少本地直连是存在的.
        logger.info('exception:%s, %s, %s' % (request.url, exception, spider.name))
        if spider.name != 'doubanSpider':
            return None

        # in direct mode, it is often time out, but just skip, not invalid dicret proxy.
        current_proxy = request.meta.get('proxy', None)
        if current_proxy is None:
            return None

        if isinstance(exception, self.DONT_RETRY_ERRORS):
            logger.info("InProxyMiddleware:process_exception: %s" % exception)

            new_request = request.copy()
            if 'Cookie' in new_request.headers.keys():
                del new_request.headers['Cookie']
            new_request.dont_filter = True
            

            invalid_count = self.proxy_status.setdefault(str(current_proxy), 0)
            self.proxy_status[str(current_proxy)] = invalid_count + 1

            new_proxy = self.get_anthor_valid_proxy(spider, current_proxy)
            self.set_proxy(new_request, new_proxy)

            return new_request



