# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


#from datetime import datetime
import hashlib
import scrapy
from scrapy.pipelines.images import ImagesPipeline
from scrapy.pipelines.files import FilesPipeline
from crawler.items import CentralBookItem
from datetime import datetime
import singleton
import config



class broadcastPipeline(object):

    def process_item(self, item, spider):

        from handler import WSHandler

        spider_info = spider.spider_info(spider)
        WSHandler.broadcast_ws_spider(spider_info)

        return item


class proxyPipeline(object):
    proxy_col = singleton.db[config.db_col_name_proxy_ip]

    def process_item(self, item, spider):
        if spider.name == 'proxySpider':
            #print 'item:', item
            tmp = self.proxy_col.find_one({'address':item['address'], 'port':item['port']})
            if tmp is None:
                self.proxy_col.insert_one(item)
            raise scrapy.exceptions.DropItem
        else:
            return item



class filterItemPipline(object):

    def process_item(self, item, spider):
        flag = True
        collection = singleton.db[spider.name]
        
        tmp = collection.find_one({"fromurl":item['fromurl']})
        if tmp is None:
            collection.insert_one(item)
            spider.spider_insert_count = spider.spider_insert_count + 1
            flag = False
        else:
            tmp['changed'] = False
            newItem = self._join_Item(tmp, item)
            if newItem['changed']:
                collection.replace_one({'fromurl':tmp['fromurl']}, newItem)
                spider.spider_update_count = spider.spider_update_count + 1
                flag = False

        if not flag:
            update_item = collection.find_one({"fromurl":item['fromurl']})
            return update_item
        else:
            #print 'DropItem'
            spider.spider_drop_count = spider.spider_drop_count + 1
            raise scrapy.exceptions.DropItem


    def _join_Item(self, oldItem, newItem):
        changed = False
        for key in oldItem.keys():
            if cmp(oldItem[key], '') == 0 and cmp(newItem[key], '') != 0:
                oldItem[key] = newItem[key]
                changed = True

        for coverurl in newItem['coverurls']:
            if coverurl not in oldItem['coverurls']:
                oldItem['coverurls'].append(coverurl)
                changed = True

        for downloadurl in newItem['downloadurls']:
            exist = False
            for old_downloadurl in oldItem['downloadurls']:
                if old_downloadurl[0] == downloadurl[0]:
                    exist = True

            if not exist:
                oldItem['downloadurls'].append(downloadurl)
                changed = True

        if (len(oldItem['coverstoragepath'])==0 and len(oldItem['coverurls']) !=0 ) or \
            (len(oldItem['bookstoragepath'])==0 and len(oldItem['downloadurls']) !=0 ) :
                changed = True

        oldItem['changed'] = changed

        return oldItem

class coverImagesPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        for cover_url in item['coverurls']:
            if cover_url.startswith("http"):
                yield scrapy.Request(cover_url, meta={'item':item})

    def item_completed(self, results, item, info):
        image_paths = [(x['path'], x['url']) for ok, x in results if ok]
        if image_paths:
            item['coverstoragepath'] = image_paths
            collection = singleton.db[info.spider.name]
            collection.replace_one({'fromurl':item['fromurl']}, item)
        return item

    def file_path(self, request, response=None, info=None):
        item = request.meta['item']
        image_name = hashlib.sha1(request.url).hexdigest()+ '.jpg'
        cover_img_path = info.spider.name + '/' + item['title'] + '_' + str(item['_id']) + '_' + image_name
        return cover_img_path

class ePubBooksPipeline(FilesPipeline):
    def get_media_requests(self, item, info):
        '''
            when ths spider is readColorSpider, we could not download the epubBooks, but we download something
        '''
        for download_url in item['downloadurls']:
            if download_url[0].startswith("http"):
                yield scrapy.Request(download_url[0], meta={'item':item, 'download_file_name': download_url[1]})

    def item_completed(self, results, item, info):
        file_paths = [(x['path'], x['url']) for ok, x in results if ok]
        if file_paths:
            item['bookstoragepath'] = file_paths
            collection = singleton.db[info.spider.name]
            collection.replace_one({'fromurl':item['fromurl']}, item)
        return item

    def file_path(self, request, response=None, info=None):
        item = request.meta['item']
        download_file_name = request.meta['download_file_name']
        file_name = download_file_name
        file_path = info.spider.name + '/' + item['title'] + '_' + str(item['_id']) + '_' + file_name
        return file_path

class integrateItemPipeline(object):

    update_keys = {'synopsises':'synopsis', 'categories':'category', 'author_introes' : 'author_intro', 
                    'scores':'score', 'tags':'tags', 'ISBNes' : 'ISBN'}

    download_update_keys = {'covers' : 'coverstoragepath', 'books': 'bookstoragepath'}
    central_book_col = singleton.db[config.db_col_name_central_book]
    central_book_col.create_index('insert_time')

    def process_item(self, item, spider):
        #print 'item:', item
        tmp = self.central_book_col.find_one({'author':item['author'], 'title':item['title']})
        if tmp is None:
            centralItem = self._init_item(item, spider)
            self.central_book_col.insert_one(centralItem)
            spider.central_insert_count = spider.central_insert_count + 1
        else:
            tmp['changed'] = False
            joined_item = self._join_item(tmp, item, spider)
            if joined_item['changed']:
                self.central_book_col.replace_one({'author':tmp['author'], 'title':tmp['title']}, joined_item)
                spider.central_update_count = spider.central_update_count + 1
            else:
                spider.central_drop_count = spider.central_drop_count + 1

        update_item = self.central_book_col.find_one({'author':item['author'], 'title':item['title']})
        return update_item
        #raise scrapy.exceptions.DropItem

    def _init_item(self,  item, spider):
        newItem = CentralBookItem()
        newItem['author'] = item['author']
        newItem['title'] = item['title']

        item_keys = item.keys()
        for key in self.update_keys:
            mapped_key = self.update_keys[key]
            newItem[key] = []
            if mapped_key not in item_keys:
                continue
            if item[mapped_key] == '':
                continue
            newItem[key].append({'fromObjId':item['_id'], 'fromurl':item['fromurl'], 'spider':spider.name, 
                                'content':item[mapped_key], 'fitness': 0})

        for key in self.download_update_keys:
            mapped_key = self.download_update_keys[key]
            newItem[key] = []
            if len(item[mapped_key]) > 0:
                for field in item[mapped_key]:
                    newItem[key].append({'fromObjId':item['_id'], 'fromurl':item['fromurl'], 'spider':spider.name, 
                                       'localpath':field[0], 'downloadurl':field[1], 'fitness':0})

        newItem['insert_time'] = datetime.now()
        return newItem

    def _join_item(self, oldItem, newItem, spider):

        new_item_keys = newItem.keys()
        changed = False
        
        for key in self.update_keys:
            mapped_key = self.update_keys[key]

            if mapped_key not in new_item_keys:
                continue
            if newItem[mapped_key] == '' or \
                (type(newItem[mapped_key]) == type([]) and len(newItem[mapped_key]) == 0):
                continue

            items = []
            exit = False
            for item in oldItem[key]:
                if item['fromObjId'] == newItem['_id']:
                    item['content'] = newItem[mapped_key]
                    exit = True
                    changed = True
                items.append(item)
            if exit:
                oldItem[key] = items

            if not exit:
                for field in oldItem[key]:
                    if field['content'] == newItem[mapped_key]:
                        exit = True
                        break

            if not exit:
                oldItem[key].append({'fromObjId':newItem['_id'], 'fromurl':newItem['fromurl'], 'spider':spider.name, 
                                        'content':newItem[mapped_key], 'fitness': 0}) 
                changed = True       


        for key in self.download_update_keys:
            mapped_key = self.download_update_keys[key]
            for new_field in newItem[mapped_key]:
                exit = False
                for old_field in oldItem[key]:
                    if old_field['downloadurl'] == new_field[1]:
                        exit = True
                        break
                if not exit:
                    oldItem[key].append({'fromObjId':newItem['_id'], 'fromurl':newItem['fromurl'], 'spider':spider.name, 
                                         'localpath':new_field[0], 'downloadurl':new_field[1], 'fitness': 0})
                    changed = True

        oldItem['changed'] = changed       

        return oldItem







if __name__ == '__main__':
    pass



