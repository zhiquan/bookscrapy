# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html


from scrapy.item import Item, Field



class BookItem(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # 书要素
    _id = Field()
    title = Field()
    author = Field()
    coverurls = Field()
    synopsis = Field()
    downloadurls = Field()
    fromurl = Field()
    fromhost = Field()
    category = Field()
    changed = Field()
    crawlerid= Field()
    ISBN = Field()
    
    # 记录要素
    updatetime = Field()
    recordtime = Field()
    coverstoragepath = Field()
    bookstoragepath = Field()


class DoubanBookItem(Item):
    _id = Field()
    title = Field()
    author = Field()
    coverurls = Field()
    fromurl = Field()
    fromhost = Field()
    score = Field()
    synopsis = Field()
    ISBN = Field()
    author_intro = Field()
    tags = Field()
    related_urls = Field()
    category = Field()
    changed = Field()
    crawlerid= Field()

    # 记录要素
    downloadurls = Field()
    updatetime = Field()
    recordtime = Field()
    coverstoragepath = Field()
    bookstoragepath = Field()


class CentralBookItem(Item):
    _id = Field()
    title = Field()
    author = Field()

    ISBNes = Field()
    synopsises = Field()
    scores = Field()
    author_introes = Field()
    tags = Field()
    categories = Field()

    covers = Field()
    books = Field()

    insert_time = Field()
    changed = Field()




class InterfaceBookItem(Item):
    _id = Field()
    title = Field()
    author = Field()

    synopsis = Field()
    author_intro = Field()
    face = Field()
    tags = Field()
    category = Field()

    fitness = Field()
    covers = Field()
    books = Field()

    insert_time = Field()



class ProxyItem(Item):
    _id  = Field()
    address = Field()
    port = Field()
    protocol = Field()
    location = Field()
    status = Field()

    proxy_type = Field() #0:anonymity #1noanonymity
    delay = Field() # in second
    timestamp = Field()





    