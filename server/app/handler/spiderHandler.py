# -*- coding: utf-8 -*-

import tornado.web
from proxy import spiderProxy
from util.crawler_process import crawler_process


class SpiderHandler(tornado.web.RequestHandler):
	def get(self):
		spiders = spiderProxy.list()
		self.render('spider.html', spiders = spiders)

	def post(self):
		spider_name = self.get_argument('spider')
		action = self.get_argument('action')

		print spider_name, action
		#print 'spider_name:', spiderProxy.get(spider_name)
		if not spiderProxy.get(spider_name):
			info = 'There is no spider named as :%s' % spider_name
			self.write(info)
			return

		if cmp(action, 'run') == 0:
			if crawler_process.is_crawling(spider_name):
				info = '%s is already running!' % (spider_name)
				self.write(info)
			else:
				#process = CrawlerProcess(get_project_settings())
				crawler_process.crawl(spider_name)
				info = '%s have been set to run!' % (spider_name)
				self.write(info)

		elif cmp(action, 'stop') == 0:
			if crawler_process.is_crawling(spider_name):
				crawler_process.stop_crawl(spider_name)
				info = '%s have been set to stop!' % (spider_name)
				self.write(info)
			else:
				info = '%s have not been running!' % spider_name
				self.write(info)

		elif cmp(action, 'pause') == 0:
			if crawler_process.is_working(spider_name):
				crawler_process.pause_crawl(spider_name)
				info = '%s have been setted to pause' % spider_name
				self.write(info)
			else:
				info = '%s have not been running!' % spider_name
				self.write(info)

		elif cmp(action, 'resume') == 0: 
			if crawler_process.is_paused(spider_name):
				crawler_process.resume_crawl(spider_name)
				info = '%s have been setted to resume' % spider_name
				self.write(info)
			else:
				info = '%s have not been paused status!' % spider_name
				self.write(info)
		else:
			info = '%s is an invalid action' % action
			self.write(info)


		
		
