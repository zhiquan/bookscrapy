# -*- coding: utf-8 -*-

import json
from tornado import websocket

class WSHandler(websocket.WebSocketHandler):
    clients = set()
 
    def open(self):
        #print 'websocket open'
        self.clients.add(self)
        #self.write_message(json.dumps({'input': 'connected...'}))
        self.stream.set_nodelay(True)
 
    def on_close(self):
        # 客户端主动关闭
        print 'web socket closed'
        self.clients.remove(self)
    
    @classmethod
    def broadcast_ws_data(cls, data):
        '''data = {
            'type' : 'log',
            'msg' : '.....' 
        }

        data = {
            'type' : 'spider'
            'name' : spiderName,
            'status' : working/Pause/Stop
        }'''

        for client in cls.clients:
            msg_pkg = json.dumps(data)
            client.write_message(msg_pkg)


    @classmethod
    def broadcast_ws_msg(cls, msg):
        data = { 'type' : 'log', 'msg': msg }
        cls.broadcast_ws_data(data)

    @classmethod
    def broadcast_ws_spider(cls, spider):
        data = {'type':'spider', 'spider':spider}
        cls.broadcast_ws_data(data)