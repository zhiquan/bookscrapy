# -*- coding: utf-8 -*-

import tornado.web
import tornado
from proxy import bookProxy
import config

class BookHandler(tornado.web.RequestHandler):
	def get(self, *args, **kwargs):
		#print 'args:', args, self.request.path[:15]
		if self.request.path[:15] == '/book/category/':

			category = '' if len(args)==0 else args[0]

			books = bookProxy.listByCategory(category)
			self.render('book.html', books = books, total_count = 0, start=0, search='', page_count = 0)

		elif self.request.path[:13] == '/book/author/':
			author = '' if len(args)==0 else args[0]
			books = bookProxy.listByAuthor(author)
			self.render('book.html', books = books, total_count = 0, start=0, search='', page_count = 0)

		else:
			book_id = '' if len(args)==0 else args[0]
			start_index = self.get_query_argument('start', default=0)
			search = self.get_query_argument('search', default='')
			print 'start_index:', start_index, 'book_id', book_id, 'search:', search
			if book_id != '':
				book = bookProxy.get(book_id)
				self.render('bookDtl.html', book=book)
			elif search != '':
				books = bookProxy.search(start_index, search)
				total_count = bookProxy.count()
				#print 'search:', total_count, len(books), books
				self.render('book.html', books = books, total_count = total_count, 
							start=int(start_index), search=search, page_count = config.book_per_page_count)
			else:
				books = bookProxy.list(start_index)
				total_count = bookProxy.count()
				#print total_count
				self.render('book.html', books = books, total_count = total_count, 
							start=int(start_index), search=search, page_count = config.book_per_page_count)

	def post(self, *args, **kwargs):
		book_id = args[0]
		update_type = args[1]
		from_id = args[2]
		if update_type == 'cover':
			fitness = self.get_body_argument('fitness', default=0)
			bookProxy.update_cover_fitness(book_id, from_id, fitness)

		if update_type == 'book':
			bookProxy.update_book_fitness(book_id, from_id, fitness)

		if update_type == 'brief':
			book_brief = tornado.escape.json_decode(self.request.body)
			#print book_brief
			bookProxy.update_brief(book_id, book_brief)


		self.write("ok");