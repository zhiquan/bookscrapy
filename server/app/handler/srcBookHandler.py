import tornado.web

from proxy import srcBookProxy

class SrcBookHandler(tornado.web.RequestHandler):
	def get(self, *args, **kwargs):
		title = self.get_query_argument('title', default='')
		author = self.get_query_argument('author', default='')
		#print title, author
		book = srcBookProxy.get(title, author)

		#print book
		self.render('srcBookDtl.html', book=book)


	def post(self, *args, **kwargs):
		book_id = args[0]
		update_key = args[1]
		from_id = args[2]

		fitness = self.get_body_argument('fitness', default=0)
		srcBookProxy.update_fitness(book_id, update_key, from_id, fitness)
		
		self.write("ok");