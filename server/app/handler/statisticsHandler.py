# -*- coding: utf-8 -*-

import tornado.web
from proxy import statCategoryProxy
from proxy import statAuthorProxy


class StatisticsHandler(tornado.web.RequestHandler):
	def get(self, *args, **kwargs):
		categories = statCategoryProxy.list()
		#print 'categories', categories

		authors = statAuthorProxy.list()
		#print 'authors:', authors
		self.render('statistics.html', categories=categories, authors=authors)