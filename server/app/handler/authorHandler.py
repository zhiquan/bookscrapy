import tornado.web
import tornado
from proxy import authorProxy


class AuthorHandler(tornado.web.RequestHandler):
	def get(self, *args, **kwargs):
		authors = authorProxy.list()
		self.render('author.html', authors=authors)

	def post(self, *args, **kwargs):
		#print self.request.path
		if self.request.path == '/author/new':
			author_info = tornado.escape.json_decode(self.request.body)
			if authorProxy.get_author_by_name(author_info['name']):
				self.write('The %s is already existed!' % author_info['name'])
			else:
				authorProxy.save(author_info)
				self.write('ok')
		elif self.request.path == '/author/delete':
			author_info = tornado.escape.json_decode(self.request.body)
			authorProxy.delete_by_name(author_info['name'])
			self.write('ok')