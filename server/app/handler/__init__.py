# -*- coding: utf-8 -*-


# Declare top-level shortcuts
__all__ = ['SpiderHandler', 'SpiderHisHandler', 'WSHandler', 'BookHandler', 
			'SrcBookHandler', 'SiteBookHandler','CleanHandler', 'CategoryHandler',
			'AuthorHandler','StatisticsHandler']
			
from spiderHandler import SpiderHandler
from spiderHisHandler import SpiderHisHandler
from wsHandler import WSHandler
from bookHander import BookHandler
from srcBookHandler import SrcBookHandler
from siteBookHandler import SiteBookHandler
from cleanHandler import CleanHandler
from categoryHandler import CategoryHandler
from statisticsHandler import StatisticsHandler
from authorHandler import AuthorHandler