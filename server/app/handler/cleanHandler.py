# -*- coding: utf-8 -*-

import tornado.web
import tornado
from proxy import cleanProxy, bookProxy

class CleanHandler(tornado.web.RequestHandler):
	def get(self, *args, **kwargs):
		oper_type = '' if len(args)==0 else args[0]
		clean_id = '' if len(args)==0 else args[1]
		if oper_type == 'insert':
			insert_ids = cleanProxy.get_insert_bookIds(clean_id)
			books = bookProxy.get_books_byIds(insert_ids)
			self.render('book.html', books = books, total_count = 0,start=0, search='', page_count = 0)
		elif oper_type == 'update':
			update_ids = cleanProxy.get_update_bookIds(clean_id)
			books = bookProxy.get_books_byIds(update_ids)
			self.render('book.html', books = books, total_count = 0,start=0, search='', page_count = 0)
		else:
			cleans = cleanProxy.list()
			self.render('clean.html', cleans=cleans)