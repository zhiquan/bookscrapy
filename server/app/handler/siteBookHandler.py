import tornado.web

from proxy import siteBookProxy

class SiteBookHandler(tornado.web.RequestHandler):
	def get(self, *args, **kwargs):
		#title = self.get_query_argument('title', default='')
		#author = self.get_query_argument('author', default='')
		#print title, author
		spider_name = args[0]
		book_id = args[1]
		book = siteBookProxy.get(spider_name, book_id)
		#print book
		self.render('siteBook.html', book=book)