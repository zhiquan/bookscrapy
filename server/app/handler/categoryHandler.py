import tornado.web
import tornado
from bson import json_util
from proxy import categoryProxy


class CategoryHandler(tornado.web.RequestHandler):
	def get(self, *args, **kwargs):
		categories = categoryProxy.list()
		self.render('category.html', categories=categories)

	def post(self, *args, **kwargs):
		category_info = tornado.escape.json_decode(self.request.body)
		if categoryProxy.get_category_by_name(category_info['name']):
			self.write('The %s is already existed!' % category_info['name'])
		else:
			if category_info['parent'] != '' and categoryProxy.get_category_by_name(category_info['parent']) is None:
				self.write('The parent %s is not existed' % category_info['parent'])
			else:
				categoryProxy.save(category_info)
				self.write(json_util.dumps(category_info))

	def delete(self, *args, **kwargs):
		#print args, kwargs
		category_id = args[0]
		#print category_id
		categoryProxy.delete_by_id(category_id)
		self.write('ok')
