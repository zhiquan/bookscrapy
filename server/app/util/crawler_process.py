# -*- coding: utf-8 -*-

from scrapy.crawler import CrawlerProcess, Crawler
from scrapy import signals
from datetime import datetime


#custom signals
signals.spider_paused = object()
signals.spider_resumed = object()

import logging
logger = logging.getLogger(__name__)

class FlyBookCrawlerProcess(CrawlerProcess):
    """
    CrawlerProcess which sets up a global signals manager,
    assigns unique ids to each spider job, workarounds some Scrapy
    issues and provides extra stats.
    """

    def __init__(self,settings=None):
        super(FlyBookCrawlerProcess, self).__init__(settings or {})
        #self.websocket_proxy = websocket_proxy

    def crawl(self, crawler_or_spidercls, *args, **kwargs):

        crawler = crawler_or_spidercls
        if not isinstance(crawler_or_spidercls, Crawler):
            crawler = self._create_crawler(crawler_or_spidercls)

        crawler.signals.connect(self.on_spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(self.on_spider_paused, signal=signals.spider_paused)
        crawler.signals.connect(self.on_spider_resumed, signal=signals.spider_resumed)
        crawler.signals.connect(self.on_spider_closed, signal=signals.spider_closed)
        crawler.signals.connect(self.on_spider_error, signal=signals.spider_error)

        #kwargs['crawl_id'] = str(uuid.uuid1())
        #kwargs['start_time'] = ''
        #kwargs['close_time'] = ''
        #kwargs['last_idle_time'] = ''
        #kwargs['idle_count'] = 0
        #kwargs['err_count'] = 0
        #kwargs['err_info'] = ''
        #print kwargs, args
        super(FlyBookCrawlerProcess, self).crawl(crawler, *args, **kwargs)


    def stop_crawl(self, crawler_name):
        """ Stop a single crawl crawl """
        self.get_crawler(crawler_name).stop()

    def pause_crawl(self, crawler_name):
        """ Pause a crawling crawl """
        crawler = self.get_crawler(crawler_name)
        crawler.engine.pause()

        print 'spider paused start'
        crawler.signals.send_catch_log(signals.spider_paused, spider=crawler.spider)
        print 'spider paused end'


    def resume_crawl(self, crawler_name):
        """ Resume a crawling crawl """
        crawler = self.get_crawler(crawler_name)
        crawler.engine.unpause()

        print 'spider resumed start'
        crawler.signals.send_catch_log(signals.spider_resumed, spider=crawler.spider)
        print 'spider resumed end'

    def get_crawler(self, crawler_name):
        for crawler in self.crawlers:
            if getattr(crawler.spidercls, "name") == crawler_name:
                return crawler
        raise KeyError("spider is not known: %s" % crawler_name)

    def is_crawling(self, crawler_name):
        crawling = False
        try:
            self.get_crawler(crawler_name)
            crawling = True
        except Exception:
            #print e
            pass

        return crawling

    def is_paused(self, crawler_name):
        paused = False
        for crawler in self.crawlers:
            if crawler.spider.name == crawler_name and crawler.spider.status == 'idle':
                paused = True

        return paused

    def is_working(self, crawler_name):
        working = False
        for crawler in self.crawlers:
            if crawler.spider.name == crawler_name and crawler.spider.status == 'working':
                working = True

        return working

    def stop(self):
        """ Terminate the process (exit from application). """
        return super(FlyBookCrawlerProcess, self).stop()

    def on_spider_opened(self, spider):
        print 'opend', spider.name

        spider.start_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        spider.status = 'working'

        spider_info = self._spider_info(spider)
        self._propagate_spider_info(spider_info)


    def on_spider_paused(self, spider):
        print 'paused', spider.name

        spider.last_idle_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        spider.idle_count = spider.idle_count + 1
        spider.status = 'idle'
        
        spider_info = self._spider_info(spider)
        self._propagate_spider_info(spider_info)        


    def on_spider_resumed(self, spider):
        print 'resumed', spider.name

        spider.last_resume_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        spider.status = 'working'
        spider_info = self._spider_info(spider)
        self._propagate_spider_info(spider_info)

    def on_spider_closed(self, spider, reason):
        print 'closed', spider.name

        spider.status = 'closed'
        spider.close_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        spider_info = self._spider_info(spider)
        self._propagate_spider_info(spider_info)

    def on_spider_error(self, failure, response, spider):
        print 'error', spider.name, failure, response, spider
        spider.err_count = spider.err_count + 1
        spider.err_info = str(response) + str(failure)

        spider_info = self._spider_info(spider)
        self._propagate_spider_info(spider_info)

    def _spider_info(self, spider):

        spider_info = spider.spider_info(spider)

        return spider_info

    def _propagate_spider_info(self, spider_info):
        from handler.wsHandler import WSHandler
        WSHandler.broadcast_ws_spider(spider_info)
        from proxy.spiderProxy import spiderProxy
        spiderProxy.update(spider_info['name'], spider_info)

from scrapy.utils.project import get_project_settings
crawler_process = FlyBookCrawlerProcess(settings=get_project_settings())


