# -*- coding: utf-8 -*-

import logging

from handler.wsHandler import WSHandler

class WSLogHandler(logging.StreamHandler):
    """
    A handler class which allows the cursor to stay on
    one line for selected messages
    """
    def emit(self, record):
        try:
            msg = self.format(record)
            WSHandler.broadcast_ws_msg(msg)
            self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)


    def filter(self, record):
        flag = False
        if record.name == 'scrapy' and record.msg.startswith("Dropped:"):
            flag = True

        if record.name.startswith("tornado."):
            flag = True

        if not flag:
            return record 


if __name__ == '__main__':
    pass