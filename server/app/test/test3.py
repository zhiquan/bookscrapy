


from tornadoist import ProcessMixin
#import time



def sleeper(n):
    #time.sleep(n)
    return 'I just <b>time.sleep</b>d for %s seconds, outside IOLoop' % n

def on_result(response):
    print response

def my_callbacks():
    #from twisted.internet import reactor

    print 'start'

    proxminin = ProcessMixin()
    proxminin.add_task(sleeper, 1, callback = on_result)

    print 'end'


def test3():
    from twisted.internet import reactor
    reactor.callWhenRunning(my_callbacks)
    reactor.run()


if __name__ == '__main__':
    test3()