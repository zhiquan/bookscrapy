
import tcelery

def connected():
	print 'c'



tcelery.setup_nonblocking_producer()
#from twisted.internet import reactor

def addResult(a):
	print a


def test2():
	print 'hello '
	from tasks import add2
	import time
	result = add2.apply_async((2, 2))
 
	print "Waiting result...", result
	while not result.ready():
		print 'a', result.ready(), result.status
  		time.sleep(2)
  		print 'b'

   	print 'hello world'



def test1():
	print 'hello '
	from tasks import add
   	d = add.delay(4, 4)
   	print 'hello world'

   	print dir(d), d.__class__
   	d.addCallback(addResult)



from twisted.internet.defer import inlineCallbacks, Deferred

@inlineCallbacks
def my_callbacks():
    from twisted.internet import reactor
    from tasks import add

    print 'first callback'
    result = yield 1
    # yielded values that aren't deferred come right back

    print 'second callback got', result
    d = Deferred()
    reactor.callLater(5, d.callback, 2)
    result = yield d
    # yielded deferreds will pause the generator

    print 'third callback got', result
    # the result of the deferred

    d = add.delay(4, 4)
    result = yield d
    print 'fourth callback got', result

    d = Deferred()
    reactor.callLater(5, d.errback, Exception(3))

    try:
        yield d
    except Exception, e:
        result = e

    print 'fourth callback got', repr(result)
   # the exception from the deferred
    reactor.stop()


def test3():
	from twisted.internet import reactor
	reactor.callWhenRunning(my_callbacks)
	reactor.run()


if __name__ == '__main__':
	#pass
	#test1()
	test2()
	#test3()
