

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.httpclient
from tornado.web import asynchronous
from tornadoist import ProcessMixin
import time

class MainHandler(tornado.web.RequestHandler, ProcessMixin):
    def sleeper(self, n):
        time.sleep(n)
        return 'I just <b>time.sleep</b>d for %s seconds, outside IOLoop' % n

    @asynchronous
    def get(self):
        print 'start'
        self.add_task(self.sleeper, 20, callback=self.on_result)
        print 'end'

    def on_result(self, response):
        print response
        self.write(str(response))
        self.finish()

class spiderHandler(tornado.web.RequestHandler):
    def get(self):
        print 'start'
        self.write('hello world')
        print 'end'


application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/spider", spiderHandler)
    ])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
