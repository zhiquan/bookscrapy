import tcelery

def connected():
    print 'c'

tcelery.setup_nonblocking_producer()

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.httpclient
from tornado.web import asynchronous
from tasks import add2

class MainHandler(tornado.web.RequestHandler):
    @asynchronous
    def get(self):
        print 'start'
        bb = add2.apply_async(args=[3,4], callback=self.on_result)
        print 'end', bb

    def on_result(self, response):
        print response
        self.write(str(response.result))
        self.finish()


application = tornado.web.Application([
        (r"/", MainHandler),
    ])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()