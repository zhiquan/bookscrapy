import time
from celery import Celery
import json
import os

#celery = Celery('tasks',backend='mongodb://127.0.0.1:27017/1', broker='mongodb://127.0.0.1:27017/0')
#celery = Celery('tasks', broker='amqp://')
#celery.conf.CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND', 'amqp')
#celery = Celery("tasks", broker="amqp://")
celery = Celery('tasks', backend='rpc://', broker='amqp://guest:guest@localhost:5672//')

#celery = Celery('tasks', broker='amqp://guest:guest@localhost:5672//')
#celery.conf.CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND', 'amqp')


@celery.task
def sendmail(mail):
	print('sending mail to %s....' % mail['to'])
	time.sleep(2.0)
	print('mail sent.')

@celery.task
def printAddResult(a):
	#print'a:', a
	pass

@celery.task
def add1(x, y):
	time.sleep(10.0)
	return x + y

@celery.task
def add2(x, y):
	time.sleep(10.0)

	a = { 'a' : x + y }
	return json.dumps(a)

@celery.task
def add3(x, y):
	time.sleep(4.0)
	return x+y

@celery.task
def add(x, y):
	time.sleep(10.0)
	return x + y

'''from celery.signals import after_task_publish,task_success

@after_task_publish.connect
def task_sent_handler(sender=None, body=None, **kwargs):
    print('after_task_publish for task id {body[id]}'.format(
        body=body,
    ))

@task_success.connect
def task_sucess_handler(**result):
	print 'result:', result'''

if __name__ == "__main__":
    #celery.start()
    pass