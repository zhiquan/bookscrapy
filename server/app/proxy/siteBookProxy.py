# -*- coding: utf-8 -*-


from bson.objectid import ObjectId
import singleton

class SiteBookProxy(object):

    def get(self, spider_name, book_id):
    	collection = singleton.db[spider_name]
        return collection.find_one({'_id': ObjectId(book_id)})

        
siteBookProxy = SiteBookProxy()