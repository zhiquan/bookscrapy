# -*- coding: utf-8 -*-


import pymongo
from bson.objectid import ObjectId
import config
import singleton

class ProxyProxy(object):

	proxy_col = singleton.db[config.db_col_name_proxy_ip]

	def list(self):
		cursor = self.proxy_col.find()
		cursor.sort('address', pymongo.ASCENDING)
		proxys = []
		for proxy in cursor:
		    proxys.append(proxy)

		return proxys

	def list_valid_proxy(self):
		cursor = self.proxy_col.find({'status':'work'})
		cursor.sort('address', pymongo.ASCENDING)
		proxys = []
		for proxy in cursor:
		    proxys.append(proxy)

		return proxys

	def save(self, proxy_info):
		self.proxy_col.insert_one(proxy_info)
		return proxy_info

	def update(self, proxy):
		self.proxy_col.replace_one({'_id':ObjectId(proxy['_id'])}, proxy)

	def delete_by_id(self, proxy_id):
		return self.proxy_col.delete_one({'_id' : ObjectId(proxy_id)})

	def delete_by_ip_port(self, ip_port):
		address = ip_port.split(":")[0]
		port = ip_port.split(":")[1]

		return self.proxy_col.delete_one({'address':address, 'port':port})


proxyProxy = ProxyProxy()