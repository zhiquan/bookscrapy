import pymongo
#from bson.objectid import ObjectId
import config
import singleton
from model import StatAuthorModel

class StatAuthorProxy(object):

	stat_author_col = singleton.db[config.db_col_name_stat_author]

	def save_from_dict(self, authors):
		for author in authors:
			count = authors[author]
			statAuthor = StatAuthorModel()
			statAuthor['name'] = author
			statAuthor['count'] = count
			self.stat_author_col.insert_one(statAuthor)
		pass

	def list(self):
		cursor = self.stat_author_col.find()
		cursor.sort('count', pymongo.DESCENDING)
		authors = []
		for author in cursor:
		    authors.append(author)

		return authors


statAuthorProxy = StatAuthorProxy()