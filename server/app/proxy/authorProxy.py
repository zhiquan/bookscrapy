# -*- coding: utf-8 -*-


import pymongo
from bson.objectid import ObjectId
import config
import singleton

class AuthorProxy(object):

	author_col = singleton.db[config.db_col_name_author]

	def list(self):
		cursor = self.author_col.find()
		#cursor.sort('level', pymongo.ASCENDING)
		cursor.sort('name', pymongo.ASCENDING)
		authors = []
		for author in cursor:
		    authors.append(author)

		return authors


	def save(self, author_info):
		self.author_col.insert_one(author_info)
		return author_info

	def get_author_by_name(self, name):
		return self.author_col.find_one({'name':name})

	def delete_by_id(self, author_id):
		return self.author_col.delete_one({'_id' : ObjectId(author_id)})

	def delete_by_name(self, name):
		#print 'name', name
		return self.author_col.delete_one({'name':name})


authorProxy = AuthorProxy()