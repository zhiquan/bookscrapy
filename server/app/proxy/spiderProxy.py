# -*- coding: utf-8 -*-

from datetime import datetime

from model import SpiderModel

class SpiderProxy(object):
    
    import singleton
    spider_col = singleton.db['spider']
    spider_his_col = singleton.db['spider_his']

    def save_from_cls(self, spider_cls):
        spider = SpiderModel()
        spider_info = spider_cls.spider_info(spider_cls)
        for key in spider_info:
            spider[key] = spider_info[key]
        return self.spider_col.insert_one(spider)

    def reset(self, spider):
        spider['status'] = 'new'
        spider['crawl_id'] = ''
        spider['start_time'] = ''
        spider['last_idle_time'] = ''
        spider['last_resume_time'] = ''
        spider['idle_count'] = 0
        spider['err_count'] = 0
        spider['err_info'] = ''


        spider['count'] = 0
        spider['spider_insert_count'] = 0
        spider['spider_update_count'] = 0
        spider['spider_drop_count'] = 0

        spider['central_insert_count'] = 0
        spider['central_update_count'] = 0
        spider['central_drop_count'] = 0

        return spider


    def get(self,spider_name):
        spider = self.spider_col.find_one({'name':spider_name})
        return spider

    def list(self):
        spiders_cursor = self.spider_col.find({})
        spiders = []
        for spider in spiders_cursor:
            spiders.append(spider)
        return spiders

    def update(self, spider_name, spider_info):
        spider = self.get(spider_name)
        spider['from_id'] = spider['_id']
        spider['insert_time'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        del spider['_id']
        self.spider_his_col.insert_one(spider)

        if '_id' in spider_info.keys():
            del spider_info['_id']
        self.spider_col.update({'name':spider_name}, {'$set':spider_info})



spiderProxy = SpiderProxy()

            