# -*- coding: utf-8 -*-

#from datetime import datetime


import pymongo
import config
from bson.objectid import ObjectId
import re

class BookProxy(object):
    
    import singleton
    book_col = singleton.db[config.db_col_name_interface_book]


    def list(self, start_index):
        if start_index == '':
            start_index = 0
        cursor = self.book_col.find({})
        cursor.sort('insert_time',pymongo.DESCENDING)
        cursor.skip(int(start_index))
        cursor.limit(config.book_per_page_count)
        books = []
        for book in cursor:
            books.append(book)

        return books

    def listByCategory(self, category):
        category_regex = re.compile(category)
        cursor = self.book_col.find({'category.content': category_regex})
        cursor.sort('insert_time', pymongo.DESCENDING)
        books = []
        for book in cursor:
            books.append(book)

        #print 'books', books
        return books

    def listByAuthor(self, author):
        cursor = self.book_col.find({'author': author})
        cursor.sort('insert_time', pymongo.DESCENDING)
        books = []
        for book in cursor:
            books.append(book)

        #print 'books', books
        return books

    def search(self, start_index, search):
        if start_index == '':
            start_index = 0

        #author_cursor =self.book_col.find({'author': {'$regex':'.*'+search+'.*'} })
        search_regex = re.compile('.*'+search+'.*', re.IGNORECASE)
        search_cursor = self.book_col.find({ '$or' : [{'author' : search_regex}, {'title' : search_regex}] })
        search_cursor.sort('insert_time', pymongo.DESCENDING)
        search_cursor.skip(int(start_index))
        search_cursor.limit(config.book_per_page_count)
        books = []
        for book in search_cursor:
            books.append(book)
        return books


    def get(self, book_id):
        book = self.book_col.find_one({'_id': ObjectId(book_id)})
        #print 'book', book
        return book

    def count(self):
        return self.book_col.count()

    def get_books_byIds(self, ids):
        objectIds = map(lambda x : ObjectId(x), ids)
        cursor = self.book_col.find({'_id': {'$in':objectIds}})
        cursor.sort('insert_time', pymongo.DESCENDING)
        books = []
        for book in cursor:
            books.append(book)
        return books

    def update_cover_fitness(self, book_id, cover_from_id, fitness):
        book = self.book_col.find_one({'_id': ObjectId(book_id)})

        new_covers = []
        for cover in book['covers']:
            if cover['fromObjId'] == ObjectId(cover_from_id):
                cover['fitness'] = int(fitness)
            new_covers.append(cover)
        book['covers'] = new_covers
        
        self.book_col.replace_one({'_id':ObjectId(book_id)}, book)

    def update_book_fitness(self, book_id, book_from_id, fitness):
        book = self.book_col.find_one({'_id': ObjectId(book_id)})

        new_covers = []
        for cover in book['books']:
            if cover['fromObjId'] == ObjectId(book_from_id):
                cover['fitness'] = int(fitness)
            new_covers.append(cover)
        book['books'] = new_covers
        
        self.book_col.replace_one({'_id':ObjectId(book_id)}, book)

    def update_brief(self, book_id, brief):
        book = self.book_col.find_one({'_id': ObjectId(book_id)})
        book['author_intro']['fitness'] = int(brief['author_intro_fitness'])
        book['author_intro']['content'] = brief['author_intro']
        book['synopsis']['fitness'] = int(brief['synopsis_fitness'])
        book['synopsis']['content'] = brief['synopsis']
        book['category']['fitness'] = int(brief['category_fitness'])
        book['category']['content'] = brief['category']
        book['fitness'] = int(brief['fitness'])

        self.book_col.replace_one({'_id':ObjectId(book_id)}, book)
        



bookProxy = BookProxy()