# -*- coding: utf-8 -*-


import pymongo
from bson.objectid import ObjectId
import config
import singleton

class CategoryProxy(object):

	category_col = singleton.db[config.db_col_name_category]

	def list(self):
		cursor = self.category_col.find()
		#cursor.sort('level', pymongo.ASCENDING)
		cursor.sort([('parent', pymongo.ASCENDING), ('level', pymongo.ASCENDING)])
		categories = []
		for category in cursor:
		    categories.append(category)

		return categories


	def save(self, category_info):
		self.category_col.insert_one(category_info)
		return category_info

	def get_category_by_name(self, name):
		return self.category_col.find_one({'name':name})

	def delete_by_id(self, category_id):
		return self.category_col.delete_one({'_id' : ObjectId(category_id)})


categoryProxy = CategoryProxy()