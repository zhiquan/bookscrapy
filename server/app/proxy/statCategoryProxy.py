import pymongo
#from bson.objectid import ObjectId
import config
import singleton
from model import StatCategoryModel

class StatCategoryProxy(object):

	stat_category_col = singleton.db[config.db_col_name_stat_category]

	def save_from_dict(self, categories):
		for category in categories:
			count = categories[category]
			statCategory = StatCategoryModel()
			statCategory['name'] = category
			statCategory['count'] = count
			self.stat_category_col.insert_one(statCategory)


	def list(self):
		cursor = self.stat_category_col.find()
		cursor.sort('count', pymongo.DESCENDING)
		categories = []
		for category in cursor:
		    categories.append(category)

		return categories


statCategoryProxy = StatCategoryProxy()