# -*- coding: utf-8 -*-
# Declare top-level shortcuts

__all__ = ['spiderProxy', 'spiderHisProxy','bookProxy', 
			'srcBookProxy','siteBookProxy', 'cleanProxy',
			'categoryProxy', 'authorProxy', 'statCategoryProxy',
			'statAuthorProxy', 'proxyProxy']



from spiderProxy import spiderProxy
from spiderHisProxy import spiderHisProxy
from bookProxy import bookProxy
from srcBookProxy import srcBookProxy
from siteBookProxy import siteBookProxy
from cleanProxy import cleanProxy
from categoryProxy import categoryProxy
from authorProxy import authorProxy
from statCategoryProxy import statCategoryProxy
from statAuthorProxy import statAuthorProxy
from proxyProxy import proxyProxy

