# -*- coding: utf-8 -*-


import config
from model import CleanModel
import pymongo
from bson.objectid import ObjectId

class CleanProxy(object):
    
    import singleton
    clean_col = singleton.db[config.db_col_name_clean_task]

    def new_clean(self):
        clean_info = CleanModel()
        clean_info['insert_count'] = 0
        clean_info['update_count'] = 0
        clean_info['skip_count'] = 0
        clean_info['crawler_document_info'] = {}

        clean_info['insert_set'] = []
        clean_info['update_set'] = []

        clean_info['start_time'] = ''
        clean_info['end_time'] = ''
        return clean_info

    def save_clean(self, clean_info):
   		self.clean_col.insert_one(clean_info)


    def list(self):
        cursor = self.clean_col.find()
        cursor.sort('start_time', pymongo.DESCENDING)
        cleans = []
        for clean in cursor:
            del clean['insert_set']
            del clean['update_set']
            cleans.append(clean)

        return cleans


    def get_insert_bookIds(self, clean_id):
        clean = self.clean_col.find_one({'_id': ObjectId(clean_id)})
        return clean['insert_set']
        
    def get_update_bookIds(self, clean_id):
        clean = self.clean_col.find_one({'_id': ObjectId(clean_id)})
        return clean['update_set']


cleanProxy = CleanProxy()