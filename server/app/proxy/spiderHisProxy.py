# -*- coding: utf-8 -*-

#from datetime import datetime

#from model import SpiderHisModel
import pymongo
class SpiderHisProxy(object):
    
    import singleton
    spider_his_col = singleton.db['spider_his']


    def list(self, spider_name, crawl_id):
    	condition = {'name':spider_name, 'crawl_id':str(crawl_id)}
    	if crawl_id == '':
    		condition = {'name':spider_name}
        cursor = self.spider_his_col.find(condition)
        cursor.sort([('insert_time',pymongo.DESCENDING),('err_count', pymongo.DESCENDING)])
        spider_his_list = []
        for spider_his in cursor:
        	spider_his_list.append(spider_his)

        return spider_his_list
        



spiderHisProxy = SpiderHisProxy()