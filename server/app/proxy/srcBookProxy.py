# -*- coding: utf-8 -*-

from bson.objectid import ObjectId

import config

class SrcBookProxy(object):
    
    import singleton
    book_col = singleton.db[config.db_col_name_central_book]


    def get(self, title, author):
        return self.book_col.find_one({'title':title, 'author':author})

    def update_fitness(self, book_id, update_key, from_id, fitness):
    	book = self.book_col.find_one({'_id': ObjectId(book_id)})

        items = []
        for item in book[update_key]:
            if item['fromObjId'] == ObjectId(from_id):
                item['fitness'] = int(fitness)
            items.append(item)
        book[update_key] = items
        
        self.book_col.replace_one({'_id':ObjectId(book_id)}, book)
        

srcBookProxy = SrcBookProxy()