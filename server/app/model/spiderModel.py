# -*- coding: utf-8 -*-

from scrapy.item import Item, Field
from crawler.spiders.bookSpider import BookSpider


class SpiderModel(Item):
	_id = Field()
	#crawl_id = Field()
	#name = Field()
	#host_url = Field()
	#status = Field()
	#crawl_count = Field()

	#start_time = Field()
	#close_time = Field()
	#last_idle_time = Field()
	#idle_count = Field()
	#err_count = Field()
	#err_info = Field()
for key in BookSpider.spider_info(BookSpider):
	SpiderModel.fields[key] = Field()




class SpiderHisModel(SpiderModel):
	_id = Field()
	from_id = Field()
	insert_time = Field()


class CleanModel(Item):
	_id = Field()

	insert_count = Field()
	update_count = Field()
	skip_count = Field()

	crawler_document_info = Field()


	insert_set = Field()
	update_set = Field()

	start_time = Field()
	end_time = Field()


class CategoryModel(Item):
	_id = Field()

	parent = Field()
	name = Field()
	count = Field()
	level = Field()


class StatCategoryModel(Item):
	_id = Field()

	name = Field()
	count = Field()



class StatAuthorModel(Item):
	_id = Field()

	name = Field()
	count = Field()
