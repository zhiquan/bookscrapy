# -*- coding: utf-8 -*-


# Declare top-level shortcuts

__all__ = ['SpiderModel','SpiderHisModel', 'CleanModel', 
			'CategoryModel', 'StatCategoryModel', 'StatAuthorModel']



from spiderModel import SpiderModel, SpiderHisModel, CleanModel,CategoryModel,StatCategoryModel,StatAuthorModel